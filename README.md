
# The Workout App

## Overview

This project was started as a learning exercise, but we thought
we'd tackle an area of interest to us while were were at it.

(This document is written using [Gitlab Markdown syntax](https://docs.gitlab.com/ce/user/markdown.html).)

## Target Functionality

The ultimate objective is to generate a workout for a user that considers:
- their current fitness and experience level
- the amount of time that they have
- the equipment that they have at the current location
- their fitness goals, and what they have done recently

Users will be able to create a profile that defines their fitness status, goals, and the equipment they have at various locations.

The application should run well on mobile devices, but should also look good on a desktop.

An admin user will be able to define new exercises, new types of equipment, and the relationships between them.

Workouts will be generated from a database of exercises, using some fancy algorithm.

Fancier ideas: 
- voice control, especially during a workout
- exercise guidance, like pictures, tips, and videos
- reporting
- importing new exercises

## Technology Stack

Overall, we are selecting technology that is current, and which we were interested in learning more about.

We are building a single "responsive" app to run on all platforms.  Someday, we might tackle a native app for mobile, but we already have our hands full with a single app.

### Client Side Technology

- **Angular** (currently version 4) for the bulk of this single page application
- **ng-bootstrap** (Angular Bootstrap) to create a responsive application, which will work equally well across a range of devices
- **ngrx/store** provides redux-pattern management of state within the application
- **SmartAdmin** is a WrapBootstrap toolkit that provides a collection of compatible libraries and utilities to give us a leg up on getting functionality built

### Server Side Technology

- **Java** services stack
- **Hibernate** Object Relational Mapping between the Java model classes and the database
- **Jersey** for mapping services to REST services
- **Postgres** database

## Dev Tools

### Required Tools

- **Git** source control
- **NodeJS** for building and serving the client app
- **Postgres** database tools
- **Chrome browser** for testing and debugging client

### Optional Tools

- **Eclipse** (or your choice of Java IDE) for building server side code
- **Visual Studio Code** lightweight editor for client side code, with built-in GIT support
- Optional VS Code plugins:
  - **language-vscode-javascript-angular2** support for JavaScript, Typescript and Angular
  - **TSLint** enhanced warnings for Typescript
  - **Angular TypeScript & Html Snippets** lots of little code snippets with shortcuts to lay them down
  - **TypeScript Import** helps with the tedious import statements within Angular

- Optional Chrome extensions:
  - **Advanced REST client** to create and test HTTP requests
  - **Augury** tools for debugging and profiling Angular apps
  - **Redux DevTools** for viewing and working with the current Redux state of application

## Getting started

- install [Visual Studio Code](https://code.visualstudio.com/Download)
- install [git](https://github.com/git-for-windows/git/releases)
- clone the repository (from within the directory where you will be doing your work)

  git clone https://pharapiak@gitlab.com/pharapiak/WorkoutApp.git

### Getting started for client-side development

- install [node.js](https://nodejs.org/en/) , which also includes npm (Node Package Manager)
- open a command prompt for installs and serving the client app

```bash
# use node package manager to install angular-cli (command line interface for Angular)
# -g indicates "global" installation
npm install -g @angular/cli

# install json-server, which provides mock REST services in front of a 
# "database" which is just a .json file 
npm install -g json-server

# change directory to repo's client app
cd WorkoutApp/client

# Use npm to install the project dependencies to client/node_modules
npm install

# start the server
ng serve
```

- open a second command prompt for serving the mock REST services

```bash
# change directory to repo's mock server
cd WorkoutApp/mock-server

# start the mock services on the local db.json file
json-server --watch db.json

```

Navigate to [http://localhost:4200/](http://localhost:4200/) in your browser to see the app running.  (As you make code changes, they will be recompiled by ng serve, and the browser will be triggered to refresh to pull your changes.)


### Getting started for server-side development

*To do*

## Project Structure

- **docs** => project design documentation
- **client** => the Angular app
- **server** => the Java server side. This has gotten stale as we've decided to focus on the UI.
- **mock-server** => a mock database db.json file, which we serve up as RESTful services with "json-server --watch db.json".  This helps us to build stuff "the real way" in the UI, making REST calls for data, without spending time building the real services and database yet.## Further Information

## Development Practices

[See here](docs/DevelopmentPractices.md)

## Further Reading

### Project Documents

The **docs** folder holds some useful design docs, including:
- the images subfolder has pics of our whiteboard work, including the original UML diagram (that will probably be a collectors' item someday!), and some use case diagrams, which help us to figure out what to build next.
- WorkoutApp.astah contains UML diagrams, readable with [Astah Community](http://astah.net/editions/community)
- WorkoutApp.mm is a mind map of ideas, questions, conclusions, notes and pointer, created with [FreeMind](http://freemind.sourceforge.net/wiki/index.php/Download).

### Other Sources of Information

- The [angular.io](https://angular.io) site is the source of all goodness regarding Angular, including the Getting Started "Heroes" tutorial, which is a great way to get into the concepts of Angular.  The **Docs** section also has great references and some good "cookbooks" for particular topics, like routing, templates, and modules.
- The rangle.io site has a [top-to-bottom tour of Angular](https://angular-2-training-book.rangle.io/), from JavaScript to ngrx.
- We've been using ngrx/store for managing state in the client application, which takes a mental adjustment. Here are some good places to start, to understand the rationale and approach:
  - [this](https://youtu.be/mhA7zZ23Odw) is a talk by one of the guys from the Angular team, now working on ngrx
  - [getting started on coding with ngrx](https://appendto.com/2016/08/handling-the-challenge-of-shared-state-with-ngrxstore-in-angular-2/)
  - [pattern of dumb, re-usable UI components, aggregated into container components that push to and listen to the store](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)
  - the [readme on the ngrx/store repo](https://github.com/ngrx/store/blob/master/README.md) has some good links
- Observables are pervasive (we are dealing with local state, fed by ansyncronous operations).  The [ReactiveX site](http://reactivex.io/documentation/observable.html) has good documentation on this whole area.  The section on [operators](http://reactivex.io/documentation/operators.html) is a good reference. 
- This project's mind map, in docs/WorkoutApp.mm, contains a growing list of references to particular topics


