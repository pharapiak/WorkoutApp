import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs/Subscription";

import { AuthService } from "app/core/auth/auth.service";
import { AppState } from "app/app.store";
import * as appActions from "app/app.actions";
import * as userActions from 'app/core/user/store/user.actions';
import { UserSelectors } from 'app/core/user/store/index';
import { User } from "app/core/user/user";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {

  public username: string;
  public password: string;
  private currentUserSubscription: Subscription;

  public currentUser$: Observable<User>;
  public loading$: Observable<boolean>;
  public loginError$: Observable<string>;

  constructor(private store: Store<AppState>, private router: Router, private authService: AuthService) { }

  ngOnInit() {
    console.log("LoginComponent ngOnInit ");
    let previousUserName = this.authService.previousUserName;
    if (previousUserName) {
      this.username = previousUserName;
    }

    this.currentUser$ = this.store.select(UserSelectors.getCurrentUser);
    this.loading$ = this.store.select(UserSelectors.getInProgress);
    this.loginError$ = this.store.select(UserSelectors.getActionError);
    this.currentUserSubscription = this.currentUser$.subscribe(
      user => {
        if (user) {
          console.log('login success');
          let redirectUrl = 'home';
          if (this.authService.redirectUrl) {
            redirectUrl = this.authService.redirectUrl;
          }
          this.router.navigate([redirectUrl]);
        }
      }
    );
  }

  ngOnDestroy(): void {
    console.log("LoginComponent ngOnDestroy ");
    this.currentUserSubscription.unsubscribe(); // otherwise we'll end up with old subscriptions still firing if user changes 
  }

  login(event) {
    event.preventDefault();
    this.store.dispatch(new userActions.LoginAttemptAction(new userActions.LoginCredentials(this.username, this.password)));
  }

}
