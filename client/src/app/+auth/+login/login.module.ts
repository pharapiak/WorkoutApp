import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AlertModule } from "ngx-bootstrap";

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { LoadingModule } from "app/shared/loading/loading.module";

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    LoginRoutingModule,
    AlertModule,
    LoadingModule,
  ],
  declarations: [LoginComponent],
  exports: []
})
export class LoginModule { }
