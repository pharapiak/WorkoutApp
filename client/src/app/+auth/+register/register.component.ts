import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { Store } from "@ngrx/store";
import { Subscription } from "rxjs/Subscription";

import { AppState } from "app/app.store";
import { User } from "app/core/user/user";
import { UserSelectors } from "app/core/user/store";
import * as userActions from 'app/core/user/store/user.actions';
import { AuthService } from "app/core/auth/auth.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: []
})
export class RegisterComponent implements OnInit {

  public newUser: User;
  public registering$: Observable<boolean>;
  public newUserRegistered$: Observable<boolean>;
  public registrationError$: Observable<string>;
  private newUserSubscription: Subscription;
  public agreedToTermsAndConditions: boolean;

  // validator options for saBootstrap directive  
  validatorOptions = {
    container: '#messages',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      email: {
        validators: {
          notEmpty: {
            message: 'Email address is required.'
          },
          emailAddress: {
            message: 'Email address is not valid.'
          }
        }
      },
      username: {
        validators: {
          notEmpty: {
            message: 'Please create a unique user name (or use your email address).'
          },
          stringLength: {
            min: 3,
            max: 30,
            message: 'User name must be between 3 and 30 characters long.'
          },
          regexp: {
            regexp: '^[a-zA-Z0-9_\.@-]+$',
            message: 'User name can only consist of alphabetical, number, dot, dash, @ and underscore.'
          }
        }
      },
      password: {
        validators: {
          notEmpty: {
            message: 'The password is required and cannot be empty.'
          },
          stringLength: {
            min: 5,
            max: 30,
            message: 'Password must be between 5 and 30 characters long.'
          },
          different: {
            field: 'username',
            message: 'Your password must be different than your user name.'
          },
        }
      },
      passwordConfirm: {
        validators: {
          notEmpty: {
            message: 'Please confirm your password by typing it a second time.'
          },
          identical: {
            field: 'password',
            message: "Your two passwords don't match"
          },
        }
      },
      terms: {
        validators: {
          notEmpty: {
            message: 'Please confirm that you agree with the terms and conditions.'
          }
        }
      },
    }
  };

  constructor(private store: Store<AppState>, private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.newUser = new User();
    this.registering$ = this.store.select(UserSelectors.getInProgress);
    this.registrationError$ = this.store.select(UserSelectors.getActionError);
    this.newUserRegistered$ = this.store.select(UserSelectors.getNewUserRegistered);
    this.newUserSubscription = this.newUserRegistered$.subscribe(
      userRegistered => {
        if (userRegistered) {
          console.log('registration success');
          this.authService.previousUserName = this.newUser.username; // use this to fill in the field for next login on this browser

          this.router.navigate(['auth/login']);
        }
      }
    );

  }

  register(event) {
    event.preventDefault();
    if (!this.agreedToTermsAndConditions) {
      this.store.dispatch(new userActions.RegisterErrorAction("Please agree to the terms and conditions."));
    } else {
      this.store.dispatch(new userActions.RegisterAction(this.newUser));
    }
  }

}
