import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AlertModule } from "ngx-bootstrap";

import { SharedModule } from "app/shared/shared.module";

// import { LoadingModule } from "app/shared/loading/loading.module";
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
// import { SmartadminValidationModule } from "app/shared/forms/validation/smartadmin-validation.module";
// import { SmartadminInputModule } from "app/shared/forms/input/smartadmin-input.module";

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    RegisterRoutingModule,
    AlertModule,
    // LoadingModule,
    SharedModule,
    // SmartadminValidationModule, // maybe is included in SharedModule
    // SmartadminInputModule,// maybe is included in SharedModule
  ],
  declarations: [RegisterComponent]
})
export class RegisterModule { }
