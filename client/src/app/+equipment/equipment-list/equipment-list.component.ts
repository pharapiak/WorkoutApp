import { Component, Input, OnInit } from '@angular/core';
import { Equipment } from 'app/core/equipment/equipment';

@Component({
  selector: 'app-equipment-list',
  templateUrl: './equipment-list.component.html',
  styleUrls: ['./equipment-list.component.css']
})
export class EquipmentListComponent implements OnInit {
  @Input()
  equipment: Equipment[];

  constructor() { }

  ngOnInit() {
  }

}
