import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
//import { EquipmentSearchService } from './equipment-search.service';
import { Equipment } from 'app/core/equipment/equipment';
@Component({
    //moduleId: module.id,
    selector: 'equipment-search',
    templateUrl: 'equipment-search.component.html',
    styleUrls: ['equipment-search.component.css'],
    //providers: [EquipmentSearchService]
})
export class EquipmentSearchComponent implements OnInit {
    @Output() filterChange: EventEmitter<string> = new EventEmitter();
    equipment: Observable<Equipment[]>;
    private searchTerms = new Subject<string>();
    constructor(
        //private equipmentSearchService: EquipmentSearchService,
        private router: Router) { }
    // Push a search term into the observable stream.
    search(term: string): void {
        //this.searchTerms.next(term);
        this.filterChange.emit(term);
    }
    ngOnInit(): void {
        // this.equipment = this.searchTerms
        //     .debounceTime(300)        // wait for 300ms pause in events
        //     .distinctUntilChanged()   // ignore if next search term is same as previous
        //     .switchMap(term => term   // switch to new observable each time
        //         // return the http search observable
        //         ? this.equipmentSearchService.search(term)
        //         // or the observable of empty heroes if no search term
        //         : Observable.of<Equipment[]>([]))
        //     .catch(error => {
        //         // TODO: real error handling
        //         console.log(error);
        //         return Observable.of<Equipment[]>([]);
        //     });
    }
    gotoDetail(equipment: Equipment): void {
        let link = ['/detail', equipment.id];
        this.router.navigate(link);
    }
}
