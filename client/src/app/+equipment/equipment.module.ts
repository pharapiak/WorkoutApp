import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularDualListBoxModule } from 'angular-dual-listbox';

import { SharedModule } from '../shared/shared.module';
import { equipmentRouting } from './equipment.routing';
import { EquipmentComponent } from './equipment/equipment.component';
import { EquipmentListComponent } from './equipment-list/equipment-list.component';
import { EquipmentSearchComponent } from './equipment-search/equipment-search.component';
import { FilterPipe } from './equipment/filter.pipe';


@NgModule({
    imports: [
        CommonModule,
        AngularDualListBoxModule,
        equipmentRouting,
        SharedModule,
    ],
    exports: [EquipmentComponent],
    providers: [],
    declarations: [EquipmentComponent, EquipmentListComponent, EquipmentSearchComponent, FilterPipe]
})
export class EquipmentModule {
}
