import { Routes, RouterModule } from '@angular/router';
import { EquipmentComponent } from './equipment/equipment.component';
import { AuthGuard } from "app/shared/auth/auth.guard";

export const equipmentRoutes: Routes = [
    {
        path: '',
        component: EquipmentComponent,
        canActivate: [AuthGuard],
        data: {
            pageTitle: 'Equipment'
        }
    }
];

export const equipmentRouting = RouterModule.forChild(equipmentRoutes);

