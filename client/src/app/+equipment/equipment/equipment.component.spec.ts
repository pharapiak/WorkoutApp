/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Store, StoreModule } from "@ngrx/store";

import { appReducer, AppState } from 'app/app.store';
import { EquipmentComponent } from './equipment.component';
import { EquipmentService } from "app/core/equipment/equipment.service";
import { CoreModule } from "app/core/core.module";
import { EquipmentCoreModule } from "app/core/equipment/equipment-core.module";

describe('Component: Equipment', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        EquipmentCoreModule
      ],
      providers: [
        // { provide: ActivatedRoute, useValue: activatedRoute },
        // { provide: Router, useClass: RouterStub },
        // { provide: EquipmentService, useValue: new MockEquipmentService() },
        // { provide: Store, useValue: StoreModule.provideStore(appReducer) }
        // messing with merge request
      ]
    });
  });

  it('should create an instance', () => {
    let component = TestBed.createComponent(EquipmentComponent);
    expect(component).toBeTruthy();
  });

});


