import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { DualListComponent } from "angular-dual-listbox/index";

import { Equipment } from 'app/core/equipment/equipment';


declare var $: any;

@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EquipmentComponent implements OnInit {

  key = 'id';
  display = 'name';
  format: any = DualListComponent.DEFAULT_FORMAT;
  filter = true;

  @Input() allEquipment: Equipment[];  // all equipment in the database
  @Input() locationName: string;


  @Output() onEquipmentChanged = new EventEmitter();

  private currentFilterTerm = "";
  private localLocationEquipment = new Array<Equipment>();

  // equipment tied to the current location
  @Input() set locationEquipment(equipment: Equipment[]) {
    this.localLocationEquipment = Object.assign(new Array<Equipment>(), equipment);
  }

  constructor() { }

  ngOnInit() {
    this.format.direction = DualListComponent.RTL;
    this.format.remove = this.locationName;
    this.format.add = "";
  }

  onFilterChange(term: string) {
    this.currentFilterTerm = term;
  }

  saveChanges(e: any) {
    this.onEquipmentChanged.emit(this.localLocationEquipment);
  }


}

