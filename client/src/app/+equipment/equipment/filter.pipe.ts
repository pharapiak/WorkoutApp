import { Pipe, PipeTransform } from '@angular/core';

import { Equipment } from 'app/core/equipment/equipment';

@Pipe({ name: 'equipmentfilter' })
export class FilterPipe implements PipeTransform {
    transform(value: Equipment[], filterTerm: string): Equipment[] {
        return value.filter(equipment => equipment.name.includes(filterTerm, 0));
    }
}