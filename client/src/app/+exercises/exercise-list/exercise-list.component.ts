import { Component, Input, OnInit } from '@angular/core';
import { Exercise } from 'app/core/exercise/exercise';

@Component({
  selector: 'app-exercise-list',
  templateUrl: './exercise-list.component.html',
  styleUrls: ['./exercise-list.component.css']
})
export class ExerciseListComponent implements OnInit {
  @Input()
  exercises: Exercise[];

  constructor() { }

  ngOnInit() {
  }

}
