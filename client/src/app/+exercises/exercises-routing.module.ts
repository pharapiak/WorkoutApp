import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExercisesComponent } from './exercises/exercises.component';
import { AuthGuard } from "app/shared/auth/auth.guard";

const routes: Routes = [
    {
        path: '',
        component: ExercisesComponent,
        canActivate: [AuthGuard],
        data: {
            pageTitle: 'Exercises'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class ExercisesRoutingModule { }


