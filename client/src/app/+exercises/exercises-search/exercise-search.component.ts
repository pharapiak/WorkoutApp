import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
//import { ExerciseSearchService } from './exercise-search.service';
import { Exercise } from 'app/core/exercise/exercise';
@Component({
    //moduleId: module.id,
    selector: 'exercise-search',
    templateUrl: 'exercise-search.component.html',
    styleUrls: ['exercise-search.component.css'],
    //providers: [ExerciseSearchService]
})
export class ExerciseSearchComponent implements OnInit {
    @Output() filterChange: EventEmitter<string> = new EventEmitter();
    exercises: Observable<Exercise[]>;
    private searchTerms = new Subject<string>();
    constructor(
        //private exerciseSearchService: ExerciseSearchService,
        private router: Router) { }
    // Push a search term into the observable stream.
    search(term: string): void {
        //this.searchTerms.next(term);
        this.filterChange.emit(term);
    }
    ngOnInit(): void {
        // this.exercises = this.searchTerms
        //     .debounceTime(300)        // wait for 300ms pause in events
        //     .distinctUntilChanged()   // ignore if next search term is same as previous
        //     .switchMap(term => term   // switch to new observable each time
        //         // return the http search observable
        //         ? this.exerciseSearchService.search(term)
        //         // or the observable of empty heroes if no search term
        //         : Observable.of<Exercise[]>([]))
        //     .catch(error => {
        //         // TODO: real error handling
        //         console.log(error);
        //         return Observable.of<Exercise[]>([]);
        //     });
    }
    gotoDetail(exercise: Exercise): void {
        let link = ['/detail', exercise.id];
        this.router.navigate(link);
    }
}
