import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { ExercisesRoutingModule } from './exercises-routing.module';
import { ExercisesComponent } from './exercises/exercises.component';
import { ExerciseListComponent } from './exercise-list/exercise-list.component';
import { ExerciseSearchComponent } from './exercises-search/exercise-search.component';
import { FilterPipe } from './exercises/filter.pipe';


@NgModule({
  imports: [
    CommonModule,
    ExercisesRoutingModule,
    SharedModule,
  ],
  providers: [],
  declarations: [ExercisesComponent, ExerciseListComponent, ExerciseSearchComponent, FilterPipe]
})
export class ExercisesModule {
}
