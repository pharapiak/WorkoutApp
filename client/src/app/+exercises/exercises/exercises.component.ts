import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { AppState } from '../../app.store';

import { Exercise } from 'app/core/exercise/exercise';
import { ExerciseService } from 'app/core/exercise/exercise.service';
import * as exercisesActions from 'app/core/exercise/store/exercise.actions';
import { ExerciseSelectors } from 'app/core/exercise/store';
import { FilterPipe } from './filter.pipe';

@Component({
  selector: 'app-exercises',
  templateUrl: './exercises.component.html',
  styleUrls: ['./exercises.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExercisesComponent implements OnInit {

  private exercises$: Observable<Exercise[]>;
  private loading$: Observable<boolean>;
  private loadingError$: Observable<string>;
  private currentFilterTerm: string = "";

  constructor(
    private store: Store<AppState>,
    private exerciseService: ExerciseService
  ) { }

  ngOnInit() {
    this.exercises$ = this.store.select(ExerciseSelectors.getAllExercises);
    this.loading$ = this.store.select(ExerciseSelectors.getExercisesLoading);
    this.loadingError$ = this.store.select(ExerciseSelectors.getExercisesLoadingError);
    this.load();
  }

  load() {
    this.store.dispatch(new exercisesActions.LoadAllAction);
  }

  onFilterChange(term: string) {
    this.currentFilterTerm = term;
  }

}

