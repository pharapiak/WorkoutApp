import { Pipe, PipeTransform } from '@angular/core';

import { Exercise } from 'app/core/exercise/exercise';

@Pipe({ name: 'exercisefilter' })
export class FilterPipe implements PipeTransform {
    transform(value: Exercise[], filterTerm: string): Exercise[] {
        return value.filter(exercise => exercise.name.includes(filterTerm, 0));
    }
}