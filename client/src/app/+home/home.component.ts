import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from "@ngrx/store";
import { AppState } from "app/app.store";
import { UserSelectors } from "app/core/user/store";
import * as userActions from "app/core/user/store/user.actions";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private store: Store<AppState>, private router: Router) { }

  ngOnInit() {
  }

  startWorkout(event) {
    event.preventDefault();
    this.router.navigate(['/user-profile']); // TODO this is temporary; should go to workout, which checks for a profile
  }
}
