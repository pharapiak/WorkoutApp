import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { Location } from 'app/core/user-profile/location';

@Component({
  selector: 'app-location-create',
  templateUrl: './location-create.component.html',
  styleUrls: ['./location-create.component.css'],
})
export class LocationCreateComponent implements OnInit {
  @Input()
  location: Location;

  @Output() onSaveLocation = new EventEmitter();
  @Output() onCancelLocation = new EventEmitter();

  // validator options for saBootstrap directive
  validatorOptions = {
    container: '#messages',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      name: {
        validators: {
          notEmpty: {
            message: 'Name is required.'
          },
          stringLength: {
            min: 3,
            max: 30,
            message: 'Location name must be between 3 and 30 characters long.'
          },
        }
      }
    }
  };


  constructor() { }

  ngOnInit() {
  }

  create(event) {
    event.preventDefault();
    this.onSaveLocation.emit(event);
  }

  cancel(event) {
    event.preventDefault();
    this.onCancelLocation.emit(event);
  }

}
