import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Location } from 'app/core/user-profile/location';
import { LocationEquipment } from "app/core/user-profile/location-equipment";
import { Equipment } from "app/core/equipment/equipment";

@Component({
  selector: 'app-location-detail',
  templateUrl: './location-detail.component.html',
  styleUrls: ['./location-detail.component.css']
})
export class LocationDetailComponent {

  @Input() location: Location;
  @Input() locationEquipment: Equipment[];
  @Input() allEquipment: Equipment[];

  @Output() onSaveLocation = new EventEmitter();
  @Output() onCancelLocation = new EventEmitter();
  @Output() onEquipmentChanged = new EventEmitter();

  constructor() { }


  saveEquipment(equipment: Equipment[]) {
    this.onEquipmentChanged.emit(equipment);
  }

}
