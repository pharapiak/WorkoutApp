import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { Location } from 'app/core/user-profile/location';

@Component({
  selector: 'app-location-list',
  templateUrl: './location-list.component.html',
  styleUrls: ['./location-list.component.css'],
})
export class LocationListComponent implements OnInit {
  @Input()
  locations: Location[];

  @Output() onSelect: EventEmitter<Location> = new EventEmitter<Location>();
  @Output() onDelete: EventEmitter<Location> = new EventEmitter<Location>();

  @Input()
  public selectedLocation: Location;

  constructor() {
    console.debug(`LocationListComponent ctor`);
  }

  ngOnInit() {
    console.debug(`LocationListComponent got locations ${this.locations}, and selectedLocation ${this.selectedLocation}`)
  }

  select($event, location) {
    $event.preventDefault();
    this.selectedLocation = location;
    this.onSelect.emit(location);
  }

  delete($event, location) {
    $event.stopPropagation();
    this.onDelete.emit(location);
  }

}
