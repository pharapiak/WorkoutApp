import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from "app/shared/auth/auth.guard";

import { LocationsComponent } from './locations/locations.component';


const routes: Routes = [
    {
        path: '',
        component: LocationsComponent,
        canActivate: [AuthGuard],
        data: {
            pageTitle: 'Profile/Locations'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class LocationRoutingModule { }
