import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';

import { LocationsComponent } from './locations/locations.component';
import { LocationListComponent } from './location-list/location-list.component';
import { LocationDetailComponent } from './location-detail/location-detail.component';
import { LocationRoutingModule } from './location-routing.module';
import { LocationCreateComponent } from './location-create/location-create.component';
import { EquipmentModule } from "app/+equipment/equipment.module";

@NgModule({
  imports: [
    CommonModule,
    LocationRoutingModule,
    SharedModule,
    EquipmentModule,
  ],
  providers: [],
  declarations: [LocationsComponent, LocationListComponent, LocationDetailComponent, LocationCreateComponent]
})
export class LocationModule {
}
