import { Component, OnInit, Input, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from "rxjs/Subscription";
import { Store, Action } from '@ngrx/store';

import { AppState } from '../../app.store';
import * as locationActions from "app/core/user-profile/store/location.actions";
import * as locationEquipmentActions from "app/core/user-profile/store/location-equipment.actions";
import { Location } from 'app/core/user-profile/location';
import { LocationSelectors, LocationEquipmentSelectors } from 'app/core/user-profile/store/index';
import { LocationEquipment } from "app/core/user-profile/location-equipment";
import * as equipmentActions from 'app/core/equipment/store/equipment.actions';
import { Equipment } from "app/core/equipment/equipment";
import { EquipmentSelectors, getEquipmentById } from "app/core/equipment/store";

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocationsComponent implements OnInit, OnDestroy {

  public locations$: Observable<Location[]>;
  public locationsLoading$: Observable<boolean>;
  public locationsLoadingError$: Observable<string>;
  private locationCount = 0;

  public locationEquipment$: Observable<LocationEquipment[]>;
  public locationEquipmentloading$: Observable<boolean>;
  public locationEquipmentLoadingError$: Observable<string>;

  // mirrors locationEquipment$ with the corresponding findById on Equipment
  public locationEquipmentSimple$: Observable<Equipment[]>;

  public allEquipment$: Observable<Equipment[]>;
  public allEquipmentloading$: Observable<boolean>;
  public allEquipmentLoadingError$: Observable<string>;

  private locationsSubscription: Subscription;
  private locationEquipmentSubscription: Subscription;


  // selected location for edit (not globally, for workouts, etc.)
  public selectedLocation: Location;
  public newLocation: Location;
  private originalLocationEquipment: LocationEquipment[]; // to figure out what the editor changed


  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    console.log("LocationsComponent ngOnInit ");

    // trigger loading of global list of equipment
    this.store.dispatch(new equipmentActions.LoadAllAction);

    this.locations$ = this.store.select(LocationSelectors.getAllLocations);
    this.locationsLoading$ = this.store.select(LocationSelectors.getLocationsLoading);
    this.locationsLoadingError$ = this.store.select(LocationSelectors.getLocationsLoadingError);
    this.locationsSubscription = this.locations$.subscribe(locations => this.locationCount = locations.length);

    this.locationEquipment$ = this.store.select(LocationEquipmentSelectors.getAllLocationEquipment);
    // this is going to be fast, no point in a progress bar, but might want to display errors somewhere
    this.locationEquipmentloading$ = this.store.select(LocationEquipmentSelectors.getLocationEquipmentLoading);
    this.locationEquipmentLoadingError$ = this.store.select(LocationEquipmentSelectors.getLocationEquipmentLoadingError);

    this.allEquipment$ = this.store.select(EquipmentSelectors.getAllEquipment);
    // TODO if this takes a long time, might want to display a progress bar, but nobody will see it until we select a location
    this.allEquipmentloading$ = this.store.select(EquipmentSelectors.getEquipmentLoading);
    this.allEquipmentLoadingError$ = this.store.select(EquipmentSelectors.getEquipmentLoadingError);

    this.locationEquipmentSimple$ = Observable.combineLatest(this.locationEquipment$, this.allEquipment$
      , (locationEquipment, allEquipment) => {
        return locationEquipment.map(le => getEquipmentById(allEquipment, le.equipmentId));
      });

    this.locationEquipmentSubscription = this.locationEquipment$.subscribe(leq => {
      this.originalLocationEquipment = Object.assign(new Array<LocationEquipment>(), leq);
    }
    );
  }

  ngOnDestroy(): void {
    this.locationsSubscription.unsubscribe();
    this.locationEquipmentSubscription.unsubscribe();
  }

  delete(location) {
    this.store.dispatch(new locationActions.DeleteAction(location.id));
  }

  select(location) {
    this.selectedLocation = location;
    // TODO: this is a bit funky. We're selecting a location that might be different than the current global selected location
    // (just for editing), then loading all LocationEquipment for that location.  We should have currentEditLocation in the 
    // store, and currentEditLocationEquipment in the store, rather than allLocationEquipment
    this.store.dispatch(new locationEquipmentActions.LoadAllAction(location.id));
  }

  copyLocation(location: Location): Location {
    // return a clone of the immutable location we got from the store, so the detail component can work on it safely
    return Object.assign(new Location(), location);
  }

  saveLocation(location: Location) {
    this.store.dispatch(new locationActions.UpdateAction(location));
  }

  cancelLocation() {
    // don't need to do anything, cuz location detail component got a copy of the Location
  }

  // There's a big in here, where multiple adds or deletes will dispatch the correct number of 
  // state change actions, but only one ADD_COMPLETE or REMOVE_COMPLETE comes through, and the 
  // state only contains one change.  Maybe it's the switchMap in the core service, only 
  // catching the first response?
  saveEquipment(equipment: Equipment[]) {

    let pendingActions = new Array<Action>();

    //find all equipment that wasn't in the list before we started, submit an AddAction for each
    equipment.filter(eq => this.originalLocationEquipment.filter(leq => leq.equipmentId === eq.id).length === 0)
      .forEach(eq => {
        let newLocationEquipment = new LocationEquipment(this.selectedLocation.id, eq.id);
        pendingActions.push(new locationEquipmentActions.AddAction(newLocationEquipment));
      }
      );

    //find all equipment that was in the list before we started, but is no longer, and submit a RemoveAction for each
    this.originalLocationEquipment.filter(leq => (equipment.filter(eq => eq.id === leq.equipmentId).length === 0))
      .forEach(leq => {
        pendingActions.push(new locationEquipmentActions.DeleteAction(leq.id));
      });

    pendingActions.forEach(action => this.store.dispatch(action));
  }

  addNewLocation() {
    this.selectedLocation = null;
    this.newLocation = new Location();
    this.newLocation.name = `Location ${++(this.locationCount)}`;
  }

  saveNewLocation() {
    this.store.dispatch(new locationActions.AddAction(this.newLocation));
    this.newLocation = null;
    this.selectedLocation = this.newLocation;
  }

  cancelNewLocation() {
    this.newLocation = null;
  }
}


