import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { Store } from "@ngrx/store";

import { AppState } from "app/app.store";
import { UserProfile } from "app/core/user-profile/user-profile";
import { UserProfileSelectors } from "app/core/user-profile/store";
import { Gender } from "app/core/user-profile/gender";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile-main.component.html',
  styleUrls: ['./user-profile-main.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserProfileMainComponent implements OnInit {

  //this is just for getting something on the screen, probably doesn't belong in this component.
  private userGender$: Observable<string>;

  private userProfile$: Observable<UserProfile>;

  constructor(private store: Store<AppState>) {
    this.userProfile$ = this.store.select(UserProfileSelectors.getCurrentUserProfile);
    this.userGender$ = this.userProfile$.map(
      user => {
        return user ? Gender[user.gender] : 'unknown'; //convert from int to gender name
      }
    );
  }

  ngOnInit() {
  }

}
