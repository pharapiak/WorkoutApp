import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserProfileMainComponent } from "./user-profile-main/user-profile-main.component";
import { AuthGuard } from "app/shared/auth/auth.guard";
import { UserProfileCoreModule } from "app/core/user-profile/user-profile-core.module";

const routes: Routes = [
    {
        path: '',
        component: UserProfileMainComponent,
        canActivate: [AuthGuard],
        data: {
            pageTitle: 'Your Profile'
        }
    }
];

@NgModule({
    imports: [
        UserProfileCoreModule,
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule],
    providers: []
})
export class UserProfileRoutingModule { }
