import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileRoutingModule } from "./user-profile-routing.module";
import { UserProfileMainComponent } from './user-profile-main/user-profile-main.component';

@NgModule({
  imports: [
    CommonModule,
    UserProfileRoutingModule,
  ],
  declarations: [UserProfileMainComponent]
})
export class UserProfileModule { }

