import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainLayoutComponent } from './shared/layout/app-layouts/main-layout.component';
import { AuthLayoutComponent } from './shared/layout/app-layouts/auth-layout.component';

export const routes: Routes = [
    {
        path: '',
        component: MainLayoutComponent,
        data: { pageTitle: 'Home' },
        children: [
            {
                // TODO maybe default to workout instead of home?
                path: '', redirectTo: '/home', pathMatch: 'full',
            },
            {
                path: 'home',
                loadChildren: 'app/+home/home.module#HomeModule',
            },
            {
                path: 'exercises',
                loadChildren: 'app/+exercises/exercises.module#ExercisesModule',
            },
            {
                path: 'profile', redirectTo: '/profile/general', pathMatch: 'full',
            },
            {
                path: 'profile/general',
                loadChildren: 'app/+user-profile/user-profile.module#UserProfileModule',
            },
            {
                path: 'profile/locations',
                loadChildren: 'app/+location/location.module#LocationModule',
            },
            {
                path: 'profile/equipment',
                loadChildren: 'app/+equipment/equipment.module#EquipmentModule',
            },

            // for development, so we can catch bad routes
            {
                path: 'error404',
                loadChildren: 'app/shared/+error404/error404.module#Error404Module',
                data: { pageTitle: 'Error404' }
            },
            // {path: 'dashboard', loadChildren: 'app/+dashboard/dashboard.module#DashboardModule',data:{pageTitle: 'Dashboard'}},
            // {path: 'calendar', loadChildren: 'app/+calendar/calendar.module#CalendarModule',data:{pageTitle: 'Calendar'}},
            // {path: 'graphs', loadChildren: 'app/+graphs/graphs-showcase.module#GraphsShowcaseModule',data:{pageTitle: 'Graphs'}},
            // {path: 'maps', loadChildren: 'app/+maps/maps.module#MapsModule',data:{pageTitle: 'Maps'}},
            // {path: 'tables', loadChildren: 'app/+tables/tables.module#TablesModule',data:{pageTitle: 'Tables'}},

        ]
    },
    { path: 'auth', component: AuthLayoutComponent, loadChildren: 'app/+auth/auth.module#AuthModule' },

    // for development, so we can catch bad routes
    { path: '**', redirectTo: 'error404' }
    // for production, so we default to the home page on bad URL
    // { path: '**', redirectTo: 'home' }

];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

