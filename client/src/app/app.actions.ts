import { type } from "./util";
import { Action } from "@ngrx/store";
import { User } from "app/core/user/user";

// generic cross-model actions

export const AppActionTypes = {
    LOGIN_COMPLETE: type('Login Completed'),
    LOGOUT: type('Logout'),
};

export class LogoutAction implements Action {
    type = AppActionTypes.LOGOUT;
    payload: null;
}

export class LoginCompleteAction implements Action {
    type = AppActionTypes.LOGIN_COMPLETE;
    constructor(public payload: User) { }
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type AppActions =
    LoginCompleteAction
    | LogoutAction
    ;


