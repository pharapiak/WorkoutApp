import { createSelector } from 'reselect';
import { ActionReducer, Store } from '@ngrx/store';
import { environment } from '../environments/environment';
import { type } from './util';

/**
 * The compose function is one of our most handy tools. In basic terms, you give
 * it any number of functions and it returns a function. This new function
 * takes a value and chains it through every composed function, returning
 * the output.
 *
 * More: https://drboolean.gitbooks.io/mostly-adequate-guide/content/ch5.html
 */
import { compose } from '@ngrx/core';

/**
 * storeFreeze prevents state from being mutated. When mutation occurs, an
 * exception will be thrown. This is useful during development mode to
 * ensure that none of the reducers accidentally mutates the state.
 */
import { storeFreeze } from 'ngrx-store-freeze';

/**
 * combineReducers is another useful metareducer that takes a map of reducer
 * functions and creates a new reducer that stores the gathers the values
 * of each reducer and stores them using the reducer's key. Think of it
 * almost like a database, where every reducer is a table in the db.
 *
 * More: https://egghead.io/lessons/javascript-redux-implementing-combinereducers-from-scratch
 */
import { combineReducers, Action } from '@ngrx/store';


/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */
import * as exerciseModule from './core/exercise/store/index';

import * as equipmentModule from './core/equipment/store/index';

import * as userModule from './core/user/store/index';

import * as userProfileModule from './core/user-profile/store/index';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface AppState {
    exerciseModule: exerciseModule.ExerciseModuleState;
    equipmentModule: equipmentModule.EquipmentModuleState;
    userModule: userModule.UserModuleState;
    userProfileModule: userProfileModule.UserProfileModuleState;
}

export const initialAppState: AppState = {
    exerciseModule: exerciseModule.initialExerciseModule,
    equipmentModule: equipmentModule.initialEquipmentModule,
    userModule: userModule.initialUserModule,
    userProfileModule: userProfileModule.initialUserProfileModule,
};

/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */

const appReducers = {
    exerciseModule: exerciseModule.exerciseModule,
    equipmentModule: equipmentModule.equipmentModule,
    userModule: userModule.userModule,
    userProfileModule: userProfileModule.userProfileModule,
};

// set up dev and prod reducers (dev one makes sure that our reducers don't accidentally mutate state)
const developmentReducer: ActionReducer<AppState> = compose(storeFreeze, combineReducers)(appReducers);
const productionReducer: ActionReducer<AppState> = combineReducers(appReducers);

export function appReducer(state: AppState = initialAppState, action: any) {
    if (environment.production) {
        return productionReducer(state, action);
    } else {
        return developmentReducer(state, action);
    }
}

// Selectors
// (no global ones yet)