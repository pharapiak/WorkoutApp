import { User } from "../user/user";

// this object gets put in local storage so that logins survive app reloads
// TODO needs to be beefed up with timeouts, and eventually an OAuth token
export class AuthToken {
    public user: User;

    public constructor(aUser: User = null) {
        this.user = aUser;
    }
}