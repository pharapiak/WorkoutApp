import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import { Store } from "@ngrx/store";

import { AppState } from "app/app.store";
import { UserService } from "app/core/user/user.service";
import { User } from "app/core/user/user";
import { objectId } from "app/util";
import { AuthToken } from "app/core/auth/auth-token";
import { LoginCompleteAction, LogoutAction } from "app/app.actions";

@Injectable()
export class AuthService {

  private static previousUserKey = 'previousUser';
  private static loggedInUserKey = 'loggedInUser';

  private _currentUserToken: AuthToken;

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  constructor(private store: Store<AppState>, private userService: UserService) {
    console.log(`creating instance ${objectId(this)} of AuthService`);
  }

  // returns the user profile if authentication succeeds, otherwise throws error  
  login(username: string, password: string): Observable<User> {
    this.currentUserToken = null;

    // the double not ('!!') is a trick to convert a possibly 'undefined' result into false
    const result = !!(username && password);
    if (!result) {
      console.log(`login attempt was successful? ${!!result}`);
      throw new Error('Username or password was invalid.');
    }

    return this.userService.getByUsername(username).map(
      user => {
        if (!this.checkPassword(user, password)) {
          console.log('login attempt failed because of bad password');
          throw new Error('Username or password was invalid.');
        }
        console.log('login attempt was successful');
        localStorage.setItem(AuthService.previousUserKey, username); // use this to fill in the field for next login on this browser
        localStorage.setItem(AuthService.loggedInUserKey, JSON.stringify(new AuthToken(user)));
        return user;
      }
    ).catch((error) => {
      console.log(`login attempt failed because of error fetching a user with user name {username}.`);
      throw error.message;
    });
  }

  logout(): void {
    this.currentUserToken = null;
    this.store.dispatch(new LogoutAction());
  }

  get previousUserName(): string {
    return localStorage.getItem(AuthService.previousUserKey);
  }

  set previousUserName(value: string) {
    if (value) {
      localStorage.setItem(AuthService.previousUserKey, value);
    } else {
      localStorage.removeItem(AuthService.previousUserKey);
    }
  }

  get isLoggedIn(): boolean {
    return !!this.currentUserToken;
  }

  // return currently logged in user, or null if there is none
  get currentUser(): User {
    if (!this.currentUserToken) {
      return null;
    }
    return this.currentUserToken.user;
  }

  get currentUserToken(): AuthToken {
    if (!this._currentUserToken) {
      this._currentUserToken = this.currentLocalStorageUserToken;
      if (this._currentUserToken) {
        // we weren't logged in, but we discovered a valid auth token in localStorage,
        // so let's sync up the store by pushing the user into the state
        this.store.dispatch(new LoginCompleteAction(this._currentUserToken.user));
      }
    }
    return this._currentUserToken;
  }

  set currentUserToken(userToken: AuthToken) {
    this._currentUserToken = userToken;
    if (userToken) {
      // strip the password out before storing the token
      const noPasswordToken = new AuthToken();
      noPasswordToken.user = Object.assign(new User(), userToken.user, { password: null });
      localStorage.setItem(AuthService.loggedInUserKey, JSON.stringify(noPasswordToken));
    } else {
      localStorage.removeItem(AuthService.loggedInUserKey);
    }
  }

  // static method for use within User reducers initial state
  public get currentLocalStorageUserToken(): AuthToken {
    const strToken = localStorage.getItem(AuthService.loggedInUserKey);
    if (!strToken) {
      return null;
    }
    const jsonToken = JSON.parse(strToken);
    let result = Object.assign(new AuthToken(), jsonToken);
    result.user = UserService.parseUser(result.user);
    return result;
  }

  private checkPassword(user: User, password: string): boolean {
    return !user ? false : user.password == password;
  }
}
