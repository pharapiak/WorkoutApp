import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from "@ngrx/effects";

import { AuthService } from './auth/auth.service';

import { UserCoreModule } from "./user/user-core.module";
import { UserService } from './user/user.service';
import { UserEffects } from "./user/store/user.effects";

import { EquipmentCoreModule } from "./equipment/equipment-core.module";
import { EquipmentService } from "./equipment/equipment.service";
import { EquipmentEffects } from "./equipment/store/equipment.effects";

import { ExerciseCoreModule } from "./exercise/exercise-core.module";
import { ExerciseService } from "./exercise/exercise.service";
import { MuscleGroupService } from "./exercise/muscle-group.service";
import { ExerciseEffects } from './exercise/store/exercise.effects';
// import { MuscleGroupEffects } from './store/muscle-group.effects';

import { UserProfileCoreModule } from "./user-profile/user-profile-core.module";
import { UserProfileService } from './user-profile/user-profile.service';
import { LocationService } from "./user-profile/location.service";
import { LocationEquipmentService } from "app/core/user-profile/location-equipment.service";
import { LocationEffects } from './user-profile/store/location.effects';
import { LocationEquipmentEffects } from './user-profile/store/location-equipment.effects';
import { UserProfileEffects } from './user-profile/store/user-profile.effects';

// CoreModule gathers up the initialization of those services that should be done only in one place, 
// to avoid the creation of multiple instances of those services (like if they are injected into a lazy load module.)
// For example, if AuthenticationServices got created twice, they might have different ideas of the currently logged in user.
// This module should not be imported from anywhere but in the root AppModule
@NgModule({
  imports: [
    CommonModule,
    EquipmentCoreModule,
    ExerciseCoreModule,
    UserCoreModule,
    UserProfileCoreModule,
    EffectsModule.run(UserEffects),
    EffectsModule.run(EquipmentEffects),
    EffectsModule.run(ExerciseEffects),
    // EffectsModule.run(MuscleGroupEffects),
    EffectsModule.run(UserProfileEffects),
    EffectsModule.run(LocationEffects),
    EffectsModule.run(LocationEquipmentEffects),
  ],
  declarations: [],
  providers: [
    AuthService,
    EquipmentService,
    ExerciseService,
    MuscleGroupService,
    ExerciseService,
    UserService,
    UserProfileService,
    LocationService,
    LocationEquipmentService,
  ]
})
export class CoreModule {

  // this is a guard against another instance of this module being created, E.g. by import in a lazy load module
  // If the constructor executes as intended, in the AppModule, there is no ancestor injector that could 
  // provide an instance of CoreModule. If this is imported into a child of AppModule, parentModule will have a value, 
  // and we'll throw an exception.
  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
    }
  }

}
