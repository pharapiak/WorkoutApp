import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http } from '@angular/http';
import { Equipment } from './equipment'

@Injectable()
export class EquipmentService {
  private contextId: string;
  private BASE_URL = 'http://localhost:3000';
  // private BASE_URL = 'http://wger.de/api/v2';

  constructor(
    private http: Http,
  ) {
  }



  public getAll(): Observable<Array<Equipment>> {
    let url = `${this.BASE_URL}/equipment`;
    // let url = `${this.BASE_URL}/equipment/?format=json`;
    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .map(result => result.json());
  }

  public get(id: string): Observable<Equipment> {
    let url = `${this.BASE_URL} / equipment / ${id}`;
    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .map(result => result.json());
  }

  public add(equipment: Equipment): Observable<Equipment> {
    console.log('add equipment');
    console.dir(equipment);
    let url = `${this.BASE_URL} / equipment`;
    return this.http
      // .post(url, equipment, { headers: this.stateService.headers })
      .post(url, equipment)
      .map(res => res.json() || {});
    //.catch(err => { });
  }

  public delete(equipmentId: string): Observable<string> {
    let url = `${this.BASE_URL} / equipment / ${equipmentId}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .delete(url)
      .map(result => { return equipmentId });
  }

  public save(equipment: Equipment) {
    let url = `${this.BASE_URL} / equipment / ${equipment.id}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .put(url, equipment)
      .map(result => result.json);
  }



}



