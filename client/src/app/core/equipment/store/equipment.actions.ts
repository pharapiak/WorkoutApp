import { Action } from '@ngrx/store';
import { type } from 'app/util';

import { Equipment } from 'app/core/equipment/equipment';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 * 
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique. 
 */
export const EquipmentActionTypes = {
    LOAD_ALL: type('[Equipment] Load All'),
    LOAD_ALL_COMPLETE: type('[Equipment] Load All Complete'),
    LOAD_ALL_ERROR: type('[Equipment] Load All Error'),
    LOAD: type('[Equipment] Load'),
    LOAD_COMPLETE: type('[Equipment] Load Complete'),
    LOAD_ERROR: type('[Equipment] Load Error'),
    ADD: type('[Equipment] Add'),
    ADD_COMPLETE: type('[Equipment] Add Complete'),
    ADD_ERROR: type('[Equipment] Add Error'),
    DELETE: type('[Equipment] Delete'),
    DELETE_COMPLETE: type('[Equipment] Delete Complete'),
    DELETE_ERROR: type('[Equipment] Delete Error'),
    UPDATE: type('[Equipment] Update'),
    UPDATE_COMPLETE: type('[Equipment] Update Complete'),
    UPDATE_ERROR: type('[Equipment] Update Error'),
    SELECT: type('[Equipment] Select'),
    SELECT_COMPLETE: type('[Equipment] Select Complete')
};


/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful 
 * type checking in reducer functions.
 * 
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
// Load All
export class LoadAllAction implements Action {
    type = EquipmentActionTypes.LOAD_ALL;
    payload: null;
}
export class LoadAllCompleteAction implements Action {
    type = EquipmentActionTypes.LOAD_ALL_COMPLETE;
    constructor(public payload: Equipment[]) { }
}
export class LoadAllErrorAction implements Action {
    type = EquipmentActionTypes.LOAD_ALL_ERROR;
    constructor(public payload: string) { }
}
// Load
export class LoadAction implements Action {
    type = EquipmentActionTypes.LOAD;
    constructor(public payload: string) { }
}
export class LoadCompleteAction implements Action {
    type = EquipmentActionTypes.LOAD_COMPLETE;
    constructor(public payload: Equipment) { }
}
export class LoadErrorAction implements Action {
    type = EquipmentActionTypes.LOAD_ERROR;
    constructor(public payload: string) { }
}
// Add
export class AddAction implements Action {
    type = EquipmentActionTypes.ADD;
    constructor(public payload: Equipment) { }
}
export class AddCompleteAction implements Action {
    type = EquipmentActionTypes.ADD_COMPLETE;
    constructor(public payload: Equipment) { }
}
export class AddErrorAction implements Action {
    type = EquipmentActionTypes.ADD_ERROR;
    constructor(public payload: string) { }
}
// Delete
export class DeleteAction implements Action {
    type = EquipmentActionTypes.DELETE;
    constructor(public payload: string) { }
}
export class DeleteCompleteAction implements Action {
    type = EquipmentActionTypes.DELETE_COMPLETE;
    constructor(public payload: string) { }
}
export class DeleteErrorAction implements Action {
    type = EquipmentActionTypes.DELETE_ERROR;
    constructor(public payload: string) { }
}
// Update
export class UpdateAction implements Action {
    type = EquipmentActionTypes.UPDATE;
    constructor(public payload: Equipment) { }
}
export class UpdateCompleteAction implements Action {
    type = EquipmentActionTypes.UPDATE_COMPLETE;
    constructor(public payload: Equipment) { }
}
export class UpdateErrorAction implements Action {
    type = EquipmentActionTypes.UPDATE_ERROR;
    constructor(public payload: string) { }
}
// Select
export class SelectAction implements Action {
    type = EquipmentActionTypes.SELECT;
    constructor(public payload: string) { }
}
export class SelectCompleteAction implements Action {
    type = EquipmentActionTypes.SELECT_COMPLETE;
    constructor(public payload: string) { }
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions =
    LoadAction
    | LoadCompleteAction
    | LoadErrorAction
    | LoadAllAction
    | LoadAllCompleteAction
    | LoadAllErrorAction
    | AddAction
    | AddCompleteAction
    | AddErrorAction
    | DeleteAction
    | DeleteCompleteAction
    | DeleteErrorAction
    | UpdateAction
    | UpdateCompleteAction
    | UpdateErrorAction
    | SelectAction
    | SelectCompleteAction
    ;
