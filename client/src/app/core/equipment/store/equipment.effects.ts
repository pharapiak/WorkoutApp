import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';

// import { GoogleBooksService } from '../services/google-books';
// import * as book from '../actions/book';
import * as equipmentActions from './equipment.actions';
import { EquipmentService } from 'app/core/equipment/equipment.service';

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application. StateUpdates is an observable of the latest state and
 * dispatched action. The `toPayload` helper function returns just
 * the payload of the currently dispatched action, useful in
 * instances where the current state is not necessary.
 *
 * If you are unfamiliar with the operators being used in these examples, please
 * check out the sources below:
 *
 * Official Docs: http://reactivex.io/rxjs/manual/overview.html#categories-of-operators
 * RxJS 5 Operators By Example: https://gist.github.com/btroncone/d6cf141d6f2c00dc6b35
 */

@Injectable()
export class EquipmentEffects {

    @Effect()
    loadAll$: Observable<Action> = this.actions$
        .ofType(equipmentActions.EquipmentActionTypes.LOAD_ALL)
        // .map((action: equipmentActions.LoadAllAction) => action.payload)
        .switchMap(() => {
            return this.equipmentService.getAll()
                .map(result => new equipmentActions.LoadAllCompleteAction(result))
                .catch((error) => of(new equipmentActions.LoadAllErrorAction(error)));
        });

    @Effect()
    load$: Observable<Action> = this.actions$
        .ofType(equipmentActions.EquipmentActionTypes.LOAD)
        .map((action: equipmentActions.LoadAction) => action.payload)
        .switchMap((id) => {
            return this.equipmentService.get(id)
                .map(result => new equipmentActions.LoadCompleteAction(result))
                .catch((error) => of(new equipmentActions.LoadErrorAction(error)));
        });

    @Effect()
    add$: Observable<Action> = this.actions$
        .ofType(equipmentActions.EquipmentActionTypes.ADD)
        .map((action: equipmentActions.AddAction) => action.payload)
        .mergeMap((data) => {
            return this.equipmentService.add(data)
                .map(result => new equipmentActions.AddCompleteAction(result))
                .catch((error) => of(new equipmentActions.AddErrorAction(error)));
        });

    @Effect()
    delete$: Observable<Action> = this.actions$
        .ofType(equipmentActions.EquipmentActionTypes.DELETE)
        .map((action: equipmentActions.DeleteAction) => action.payload)
        .mergeMap((data) => {
            return this.equipmentService.delete(data)
                .map(result => new equipmentActions.DeleteCompleteAction(result))
                .catch((error) => of(new equipmentActions.DeleteErrorAction(error)));
        });

    constructor(
        private actions$: Actions,
        private equipmentService: EquipmentService
    ) { }



}
