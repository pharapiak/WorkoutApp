import { createSelector } from 'reselect';

import { Equipment } from 'app/core/equipment/equipment'
import * as equipmentActions from './equipment.actions';


export interface EquipmentState {
    equipment: Equipment[];
    selectedEquipmentId: string;
    loading: boolean;
    loadingError: string;
};

export const initialEquipmentState: EquipmentState = {
    equipment: new Array<Equipment>(),
    selectedEquipmentId: null,
    loading: false,
    loadingError: null
};

export function equipmentReducer(state: EquipmentState = initialEquipmentState, action: equipmentActions.Actions): EquipmentState {
    switch (action.type) {
        case equipmentActions.EquipmentActionTypes.LOAD_ALL: {
            return Object.assign({}, state, { loading: true, loadingError: null });
        }
        case equipmentActions.EquipmentActionTypes.ADD_COMPLETE: {
            console.log('ADD_COMPLETE');
            return Object.assign({}, state, { equipment: [...state.equipment, action.payload] });
        }
        case equipmentActions.EquipmentActionTypes.DELETE_COMPLETE: {
            return Object.assign({}, state, { equipment: state.equipment.filter(x => x.id !== action.payload) });
        }
        case equipmentActions.EquipmentActionTypes.LOAD_ALL_COMPLETE: {
            return Object.assign({}, state, { equipment: action.payload, loading: false, loadingError: null });
        }
        case equipmentActions.EquipmentActionTypes.LOAD_COMPLETE: {
            break;
        }
        case equipmentActions.EquipmentActionTypes.LOAD_ALL_ERROR: {
            return Object.assign({}, state, { loading: false, loadingError: action.payload });
        }
        case equipmentActions.EquipmentActionTypes.SELECT: {
            return Object.assign({}, state, { selectedEquipmentId: action.payload });
        }
        case equipmentActions.EquipmentActionTypes.UPDATE_COMPLETE: {
            break;
        }
        default: {
            return state;
        }
    }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */
export const getSelectedEntityId = (state: EquipmentState) => state.selectedEquipmentId;

export const getEntities = (state: EquipmentState) => state.equipment;

export const getLoading = (state: EquipmentState) => state.loading;

export const getLoadingError = (state: EquipmentState) => state.loadingError;

export const getEntityById = (state: EquipmentState, id: string) => {
    console.log(`getEntityById ${id}`);
    return state.equipment.find(x => x.id === id);
}

export const getSelectedEntity = (state: EquipmentState) => {
    console.log('getSelectedEntity');
    // console.dir(state);
    // return state.equipment.find(x => x.id === state.selectedEquipmentId);
    return getEntityById(state, state.selectedEquipmentId);
}


