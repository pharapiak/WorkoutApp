import { createSelector } from 'reselect';
import { memoize } from 'lodash.memoize';
import { ActionReducer } from '@ngrx/store';
import { environment } from 'environments/environment';
import { compose } from '@ngrx/core';
import { storeFreeze } from 'ngrx-store-freeze';
import { combineReducers } from '@ngrx/store';
import { AppState } from 'app/app.store';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */
import * as fromEquipment from './equipment.reducer';
import { Equipment } from "app/core/equipment/equipment";

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface EquipmentModuleState {
    equipment: fromEquipment.EquipmentState;
}

export const initialEquipmentModule = {
    equipment: fromEquipment.initialEquipmentState
}


/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */
const reducers = {
    equipment: fromEquipment.equipmentReducer,
    //muscleGroup: fromMuscleGroup.muscleGroupReducer
};

//set up the production vs dev reducers (dev uses storeFreeze to make sure that our reducers are not mutating state)
const developmentReducer: ActionReducer<EquipmentModuleState> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<EquipmentModuleState> = combineReducers(reducers);

export function equipmentModule(state: any, action: any) {
    if (environment.production) {
        return productionReducer(state, action);
    } else {
        return developmentReducer(state, action);
    }
}

/***************************
 * Selectors
 **************************/

const getEquipmentModuleState = (state: AppState) => state.equipmentModule;
const getEquipmentState = (state: AppState) => getEquipmentModuleState(state).equipment;

export function getEquipmentById(allEquipment: Equipment[], id: string): Equipment {
    const matches = allEquipment.filter(eq => eq.id === id);
    if (matches.length === 0) {
        return null;
    }
    if (matches.length > 1) {
        throw new Error(`Found more than one equipment with id ${id}`);
    }
    return matches[0];
}

// Equipment Selectors
export class EquipmentSelectors {
    static getAllEquipment = createSelector(getEquipmentState, fromEquipment.getEntities);
    static getSelectedEquipmentId = createSelector(getEquipmentState, fromEquipment.getSelectedEntityId);
    static getEquipmentLoading = createSelector(getEquipmentState, fromEquipment.getLoading);
    static getEquipmentLoadingError = createSelector(getEquipmentState, fromEquipment.getLoadingError);
    static getSelectedEquipment = createSelector(getEquipmentState, fromEquipment.getSelectedEntity);
}

