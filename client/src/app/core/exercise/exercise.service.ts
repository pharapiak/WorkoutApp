import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http } from '@angular/http';
import { Exercise } from './exercise'

@Injectable()
export class ExerciseService {
  private contextId: string;
  private BASE_URL = 'http://localhost:3000/exercises';
  // private BASE_URL = 'http://wger.de/api/v2';

  constructor(
    private http: Http,
  ) {
  }



  public getAll(): Observable<Array<Exercise>> {
    let url = `${this.BASE_URL}`;
    // let url = `${this.BASE_URL}/exercise/?format=json`;
    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .map(result => result.json());
  }

  public get(id: string): Observable<Exercise> {
    let url = `${this.BASE_URL}/${id}`;
    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .map(result => result.json());
  }

  public create(exercise: Exercise): Observable<Exercise> {
    console.log('create exercise');
    console.dir(exercise);
    let url = `${this.BASE_URL}`;
    return this.http
      // .post(url, exercise, { headers: this.stateService.headers })
      .post(url, exercise)
      .map(res => res.json() || {});
    //.catch(err => { });
  }

  public delete(exerciseId: string): Observable<string> {
    let url = `${this.BASE_URL}/${exerciseId}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .delete(url)
      .map(result => { return exerciseId });
  }

  public save(exercise: Exercise) {
    let url = `${this.BASE_URL}/${exercise.id}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .put(url, exercise)
      .map(result => result.json);
  }

}



