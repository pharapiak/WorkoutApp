
import { ModelBase } from '../../shared/model/model'

export class Exercise extends ModelBase {
    public name: string;
    public description: string;
    public tips: string;
    public recommendedReps: number;
    public expectedDuration: number;
    public difficulty: number;
    public noiseLevel: number;
    // public pictures: Url[];
    // public video: Url;
    public muscleGroups: string[]; //list of ids
}