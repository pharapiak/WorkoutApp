/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MuscleGroupService } from './muscle-group.service';

describe('Service: MuscleGroup', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MuscleGroupService]
    });
  });

  it('should ...', inject([MuscleGroupService], (service: MuscleGroupService) => {
    expect(service).toBeTruthy();
  }));
});
