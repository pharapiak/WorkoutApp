import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import { MuscleGroup } from './muscle-group';



// TODO convert this to REST
@Injectable()
export class MuscleGroupService {

  // since muscle groups don't change, I'm taking a shortcut and hard-coding the 
  // list here
  private allMuscleGroups: MuscleGroup[];

  constructor() {
    this.loadAllMuscleGroups();
  }

  public getAll(): MuscleGroup[] {
    return this.allMuscleGroups;
  }

  public get(id: string): MuscleGroup {
    return this.allMuscleGroups.find(mg => mg.id === id);
  }

  private loadAllMuscleGroups(): void {
    this.allMuscleGroups = [
      {
        id: '1',
        name: 'Abdominal',
        description: 'Stomach'
      },
      {
        id: '2',
        name: 'Biceps',
        description: 'Front of upper arm'
      },
      {
        id: '3',
        name: 'Deltoids',
        description: 'Top of shoulder'
      },
      {
        id: '4',
        name: 'Erector Spinae',
        description: 'Low back'
      },
      {
        id: '5',
        name: 'Gastronemius & Soleus',
        description: 'Back of lower leg'
      },
      {
        id: '6',
        name: 'Gluteus',
        description: 'Buttocks'
      },
      {
        id: '7',
        name: 'Hamstrings',
        description: 'Back of thigh'
      },
      {
        id: '8',
        name: 'Latissimus Dorsi & Rhomboids',
        description: 'Lat = Back, Rhom=Between shoulder blades'
      },
      {
        id: '9',
        name: 'Obliques',
        description: 'Side of body'
      },
      {
        id: '10',
        name: 'Pectoralis',
        description: 'Front of upper chest'
      },
      {
        id: '11',
        name: 'Quadriceps',
        description: 'Front of thigh'
      },
      {
        id: '12',
        name: 'Trepezius',
        description: 'Large muscle in upper and mid back'
      },
      {
        id: '13',
        name: 'Triceps',
        description: 'Back of upper arm'
      }] as MuscleGroup[];
  }
}
