
import { ModelBase } from '../../shared/model/model';

export class MuscleGroup extends ModelBase {
    public name: string;
    public description: string;
    public MuscleGroup() { }
}