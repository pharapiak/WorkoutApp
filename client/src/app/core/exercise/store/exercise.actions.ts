import { Action } from '@ngrx/store';
import { type } from 'app/util';

import { Exercise } from 'app/core/exercise/exercise';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 * 
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique. 
 */
export const ExerciseActionTypes = {
    LOAD_ALL: type('[Exercise] Load All'),
    LOAD_ALL_COMPLETE: type('[Exercise] Load All Complete'),
    LOAD_ALL_ERROR: type('[Exercise] Load All Error'),
    LOAD: type('[Exercise] Load'),
    LOAD_COMPLETE: type('[Exercise] Load Complete'),
    LOAD_ERROR: type('[Exercise] Load Error'),
    ADD: type('[Exercise] Add'),
    ADD_COMPLETE: type('[Exercise] Add Complete'),
    ADD_ERROR: type('[Exercise] Add Error'),
    DELETE: type('[Exercise] Delete'),
    DELETE_COMPLETE: type('[Exercise] Delete Complete'),
    DELETE_ERROR: type('[Exercise] Delete Error'),
    UPDATE: type('[Exercise] Update'),
    UPDATE_COMPLETE: type('[Exercise] Update Complete'),
    UPDATE_ERROR: type('[Exercise] Update Error'),
    SELECT: type('[Exercise] Select'),
    SELECT_COMPLETE: type('[Exercise] Select Complete')
};


/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful 
 * type checking in reducer functions.
 * 
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
// Load All
export class LoadAllAction implements Action {
    type = ExerciseActionTypes.LOAD_ALL;
    payload: null;
}
export class LoadAllCompleteAction implements Action {
    type = ExerciseActionTypes.LOAD_ALL_COMPLETE;
    constructor(public payload: Exercise[]) { }
}
export class LoadAllErrorAction implements Action {
    type = ExerciseActionTypes.LOAD_ALL_ERROR;
    constructor(public payload: string) { }
}
// Load
export class LoadAction implements Action {
    type = ExerciseActionTypes.LOAD;
    constructor(public payload: string) { }
}
export class LoadCompleteAction implements Action {
    type = ExerciseActionTypes.LOAD_COMPLETE;
    constructor(public payload: Exercise) { }
}
export class LoadErrorAction implements Action {
    type = ExerciseActionTypes.LOAD_ERROR;
    constructor(public payload: string) { }
}
// Add
export class AddAction implements Action {
    type = ExerciseActionTypes.ADD;
    constructor(public payload: Exercise) { }
}
export class AddCompleteAction implements Action {
    type = ExerciseActionTypes.ADD_COMPLETE;
    constructor(public payload: Exercise) { }
}
export class AddErrorAction implements Action {
    type = ExerciseActionTypes.ADD_ERROR;
    constructor(public payload: string) { }
}
// Delete
export class DeleteAction implements Action {
    type = ExerciseActionTypes.DELETE;
    constructor(public payload: string) { }
}
export class DeleteCompleteAction implements Action {
    type = ExerciseActionTypes.DELETE_COMPLETE;
    constructor(public payload: string) { }
}
export class DeleteErrorAction implements Action {
    type = ExerciseActionTypes.DELETE_ERROR;
    constructor(public payload: string) { }
}
// Update
export class UpdateAction implements Action {
    type = ExerciseActionTypes.UPDATE;
    constructor(public payload: Exercise) { }
}
export class UpdateCompleteAction implements Action {
    type = ExerciseActionTypes.UPDATE_COMPLETE;
    constructor(public payload: Exercise) { }
}
export class UpdateErrorAction implements Action {
    type = ExerciseActionTypes.UPDATE_ERROR;
    constructor(public payload: string) { }
}
// Select
export class SelectAction implements Action {
    type = ExerciseActionTypes.SELECT;
    constructor(public payload: string) { }
}
export class SelectCompleteAction implements Action {
    type = ExerciseActionTypes.SELECT_COMPLETE;
    constructor(public payload: string) { }
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions =
    LoadAction
    | LoadCompleteAction
    | LoadErrorAction
    | LoadAllAction
    | LoadAllCompleteAction
    | LoadAllErrorAction
    | AddAction
    | AddCompleteAction
    | AddErrorAction
    | DeleteAction
    | DeleteCompleteAction
    | DeleteErrorAction
    | UpdateAction
    | UpdateCompleteAction
    | UpdateErrorAction
    | SelectAction
    | SelectCompleteAction
    ;
