import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';

// import { GoogleBooksService } from '../services/google-books';
// import * as book from '../actions/book';
import * as exerciseActions from './exercise.actions';
import { ExerciseService } from 'app/core/exercise/exercise.service';

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application. StateUpdates is an observable of the latest state and
 * dispatched action. The `toPayload` helper function returns just
 * the payload of the currently dispatched action, useful in
 * instances where the current state is not necessary.
 *
 * If you are unfamiliar with the operators being used in these examples, please
 * check out the sources below:
 *
 * Official Docs: http://reactivex.io/rxjs/manual/overview.html#categories-of-operators
 * RxJS 5 Operators By Example: https://gist.github.com/btroncone/d6cf141d6f2c00dc6b35
 */

@Injectable()
export class ExerciseEffects {

    @Effect()
    loadAll$: Observable<Action> = this.actions$
        .ofType(exerciseActions.ExerciseActionTypes.LOAD_ALL)
        // .map((action: exerciseActions.LoadAllAction) => action.payload)
        .switchMap(() => {
            return this.exerciseService.getAll()
                .map(result => new exerciseActions.LoadAllCompleteAction(result))
                .catch((error) => of(new exerciseActions.LoadAllErrorAction(error)));
        });

    @Effect()
    load$: Observable<Action> = this.actions$
        .ofType(exerciseActions.ExerciseActionTypes.LOAD)
        .map((action: exerciseActions.LoadAction) => action.payload)
        .switchMap((id) => {
            return this.exerciseService.get(id)
                .map(result => new exerciseActions.LoadCompleteAction(result))
                .catch((error) => of(new exerciseActions.LoadErrorAction(error)));
        });

    @Effect()
    add$: Observable<Action> = this.actions$
        .ofType(exerciseActions.ExerciseActionTypes.ADD)
        .map((action: exerciseActions.AddAction) => action.payload)
        .mergeMap((data) => {
            return this.exerciseService.create(data)
                .map(result => new exerciseActions.AddCompleteAction(result))
                .catch((error) => of(new exerciseActions.AddErrorAction(error)));
        });

    @Effect()
    delete$: Observable<Action> = this.actions$
        .ofType(exerciseActions.ExerciseActionTypes.DELETE)
        .map((action: exerciseActions.DeleteAction) => action.payload)
        .mergeMap((data) => {
            return this.exerciseService.delete(data)
                .map(result => new exerciseActions.DeleteCompleteAction(result))
                .catch((error) => of(new exerciseActions.DeleteErrorAction(error)));
        });

    constructor(
        private actions$: Actions,
        private exerciseService: ExerciseService
    ) { }



}
