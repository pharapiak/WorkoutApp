import { createSelector } from 'reselect';

import { Exercise } from 'app/core/exercise/exercise'
import * as exerciseActions from './exercise.actions';


export interface ExerciseState {
    exercises: Exercise[];
    selectedExerciseId: string;
    loading: boolean;
    loadingError: string;
};

export const initialExerciseState: ExerciseState = {
    exercises: new Array<Exercise>(),
    selectedExerciseId: null,
    loading: false,
    loadingError: null
};

export function exerciseReducer(state = initialExerciseState, action: exerciseActions.Actions): ExerciseState {
    switch (action.type) {
        case exerciseActions.ExerciseActionTypes.LOAD_ALL: {
            return Object.assign({}, state, { loading: true, loadingError: null });
        }
        case exerciseActions.ExerciseActionTypes.ADD_COMPLETE: {
            console.log('ADD_COMPLETE');
            return Object.assign({}, state, { exercises: [...state.exercises, action.payload] });
        }
        case exerciseActions.ExerciseActionTypes.DELETE_COMPLETE: {
            return Object.assign({}, state, { exercises: state.exercises.filter(x => x.id !== action.payload) });
        }
        case exerciseActions.ExerciseActionTypes.LOAD_ALL_COMPLETE: {
            return Object.assign({}, state, { exercises: action.payload, loading: false, loadingError: null });
        }
        case exerciseActions.ExerciseActionTypes.LOAD_COMPLETE: {
            break;
        }
        case exerciseActions.ExerciseActionTypes.LOAD_ALL_ERROR: {
            return Object.assign({}, state, { loading: false, loadingError: action.payload });
        }
        case exerciseActions.ExerciseActionTypes.SELECT: {
            return Object.assign({}, state, { selectedExerciseId: action.payload });
        }
        case exerciseActions.ExerciseActionTypes.UPDATE_COMPLETE: {
            break;
        }
        default: {
            return state;
        }
    }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */
export const getSelectedEntityId = (state: ExerciseState) => state.selectedExerciseId;

export const getEntities = (state: ExerciseState) => state.exercises;

export const getLoading = (state: ExerciseState) => state.loading;

export const getLoadingError = (state: ExerciseState) => state.loadingError;

export const getSelectedEntity = (state: ExerciseState) => {
    console.log('getSelectedEntity for Exercises');
    // console.dir(state);
    return state.exercises.find(x => x.id === state.selectedExerciseId);
}

