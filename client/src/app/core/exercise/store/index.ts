import { createSelector } from 'reselect';
import { ActionReducer } from '@ngrx/store';
import { environment } from 'environments/environment';
import { compose } from '@ngrx/core';
import { storeFreeze } from 'ngrx-store-freeze';
import { combineReducers } from '@ngrx/store';
import { AppState } from 'app/app.store';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */
import * as fromExercise from './exercise.reducer';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface ExerciseModuleState {
    exercise: fromExercise.ExerciseState;
}

export const initialExerciseModule = {
    exercise: fromExercise.initialExerciseState
}


/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */
const reducers = {
    exercise: fromExercise.exerciseReducer,
    //muscleGroup: fromMuscleGroup.muscleGroupReducer
};

//set up the production vs dev reducers (dev uses storeFreeze to make sure that our reducers are not mutating state)
const developmentReducer: ActionReducer<ExerciseModuleState> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<ExerciseModuleState> = combineReducers(reducers);

export function exerciseModule(state: any, action: any) {
    if (environment.production) {
        return productionReducer(state, action);
    } else {
        return developmentReducer(state, action);
    }
}

/***************************
 * Selectors
 **************************/

const getExerciseModuleState = (state: AppState) => state.exerciseModule;
const getExerciseState = (state: AppState) => getExerciseModuleState(state).exercise;


// Exercise Selectors
export class ExerciseSelectors {
    static getAllExercises = createSelector(getExerciseState, fromExercise.getEntities);
    static getSelectedExerciseId = createSelector(getExerciseState, fromExercise.getSelectedEntityId);
    static getExercisesLoading = createSelector(getExerciseState, fromExercise.getLoading);
    static getExercisesLoadingError = createSelector(getExerciseState, fromExercise.getLoadingError);
    static getSelectedExercise = createSelector(getExerciseState, fromExercise.getSelectedEntity);
}

