
//If you're new to enums in TypeScript, here's a good article:
// https://www.gurustop.net/blog/2016/05/24/how-to-use-typescript-enum-with-angular2/
// tip: if you want to reference it in an HTML template, you have to explicitly use it in 
// the corresponding component.
export enum Gender {
    Unspecified = 0,
    Male = 1,
    Female = 2
}
