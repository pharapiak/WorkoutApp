/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LocationEquipmentService } from './location-equipment.service';

describe('Service: Location', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocationEquipmentService]
    });
  });

  it('should ...', inject([LocationEquipmentService], (service: LocationEquipmentService) => {
    expect(service).toBeTruthy();
  }));
});
