import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http } from '@angular/http';
import { LocationEquipment } from './location-equipment'

@Injectable()
export class LocationEquipmentService {
  private contextId: string;
  private BASE_URL = 'http://localhost:3000/location-equipment';

  constructor(
    private http: Http,
  ) {
  }

  public getAllForLocation(locationId: string): Observable<Array<LocationEquipment>> {
    let url = `${this.BASE_URL}?locationId=${locationId}`;
    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .map(result => result.json());
  }

  public get(locationEquipmentId: string): Observable<LocationEquipment> {
    let url = `${this.BASE_URL}/${locationEquipmentId}`;
    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .map(result => result.json());
  }

  public create(locationEquipment: LocationEquipment): Observable<LocationEquipment> {
    console.log('add equipment to location');
    console.dir(locationEquipment);
    let url = `${this.BASE_URL}`;
    return this.http
      // .post(url, location, { headers: this.stateService.headers })
      .post(url, locationEquipment)
      .map(res => res.json() || {});
    //.catch(err => { });
  }

  public delete(locationEquipmentId: string): Observable<string> {
    let url = `${this.BASE_URL}/${locationEquipmentId}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .delete(url)
      .map(result => { return locationEquipmentId });
  }

  public save(locationEquipment: LocationEquipment) {
    let url = `${this.BASE_URL}/${locationEquipment.id}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .put(url, locationEquipment)
      .map(result => result.json);
  }


}



