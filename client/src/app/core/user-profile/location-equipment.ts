
import { ModelBase } from 'app/shared/model/model'


// represents equipment available at a specific location
export class LocationEquipment extends ModelBase {
    constructor(public locationId: string = null, public equipmentId: string = null) {
        super();
    }
}