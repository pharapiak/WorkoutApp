import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http } from '@angular/http';
import { Location } from './location'

@Injectable()
export class LocationService {

  public static selectedLocationId = 'selectedLocationId';

  private BASE_URL = 'http://localhost:3000/locations';


  // derive a localStorage key for the last location the given user selected
  private static getMostRecentLocationIdLocalStorageKey(userId: string): string {
    return `${LocationService.selectedLocationId}-${userId}`;
  }

  // save the location that the user has selected, so it will be the default on next login
  public static setMostRecentLocationId(userId: string, locationId: string) {
    const key = LocationService.getMostRecentLocationIdLocalStorageKey(userId);
    if (locationId) {
      localStorage.setItem(key, locationId.toString());
    } else {
      localStorage.removeItem(key);
    }
  }

  // get the most recent location set by the given user, in a previous session
  public static getMostRecentLocationId(userId: string): string {
    const key = LocationService.getMostRecentLocationIdLocalStorageKey(userId);
    let id = localStorage.getItem(key);
    if (!id) {
      return null;
    }
    return id;
  }

  constructor(private http: Http) {
  }

  public getAllForProfile(profileId: string): Observable<Array<Location>> {
    let url = `${this.BASE_URL}?userProfileId=${profileId}`;
    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .map(result => result.json());
  }

  public get(locationId: string): Observable<Location> {
    let url = `${this.BASE_URL}/${locationId}`;
    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .map(result => result.json());
  }

  public create(location: Location): Observable<Location> {
    console.log('create location');
    console.dir(location);
    let url = `${this.BASE_URL}`;
    return this.http
      // .post(url, location, { headers: this.stateService.headers })
      .post(url, location)
      .map(res => res.json() || {});
    //.catch(err => { });
  }

  public delete(locationId: string): Observable<string> {
    let url = `${this.BASE_URL}/${locationId}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .delete(url)
      .map(result => { return locationId });
  }

  public save(location: Location) {
    let url = `${this.BASE_URL}/${location.id}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .put(url, location)
      .map(result => result.json);
  }


}



