
import { ModelBase } from 'app/shared/model/model'

export class Location extends ModelBase {
    public userProfileId: string;
    public equipmentIds: string[]; //list of ids

    constructor(public name: string = "Default") {
        super();
    }
}