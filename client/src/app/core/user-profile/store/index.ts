import { createSelector } from 'reselect';
import { ActionReducer } from '@ngrx/store';
import { compose } from '@ngrx/core';
import { storeFreeze } from 'ngrx-store-freeze';
import { combineReducers } from '@ngrx/store';

import { environment } from '../../../../environments/environment';

export { UserProfileActions } from './user-profile.actions'
export { LocationActions } from './location.actions'
export { LocationEquipmentActions } from './location-equipment.actions'

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single import name.
 */
import { AppState } from "app/app.store";
import * as fromUserProfile from './user-profile.reducer';
import * as fromLocation from './location.reducer';
import * as fromLocationEquipment from './location-equipment.reducer';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface UserProfileModuleState {
    userProfile: fromUserProfile.UserProfileState;
    location: fromLocation.LocationState;
    locationEquipment: fromLocationEquipment.LocationEquipmentState;
}

export const initialUserProfileModule: UserProfileModuleState = {
    userProfile: fromUserProfile.initialUserProfileState,
    location: fromLocation.initialLocationState,
    locationEquipment: fromLocationEquipment.initialLocationEquipmentState,
};

/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */
const reducers = {
    userProfile: fromUserProfile.userProfileReducer,
    location: fromLocation.locationReducer,
    locationEquipment: fromLocationEquipment.locationEquipmentReducer,
};

// set up the production vs dev reducers (dev uses storeFreeze to make sure that our reducers are not mutating state)
const developmentReducer: ActionReducer<UserProfileModuleState> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<UserProfileModuleState> = combineReducers(reducers);

export function userProfileModule(state: UserProfileModuleState = initialUserProfileModule, action: any) {
    if (environment.production) {
        return productionReducer(state, action);
    } else {
        return developmentReducer(state, action);
    }
}

/***************************
 * Selectors
 **************************/

const getUserProfileModuleState = (state: AppState) => state.userProfileModule;
const getUserProfileState = (state: AppState) => getUserProfileModuleState(state).userProfile;
const getLocationState = (state: AppState) => getUserProfileModuleState(state).location;
const getLocationEquipmentState = (state: AppState) => getUserProfileModuleState(state).locationEquipment;

// User Selectors
export class UserProfileSelectors {
    static getCurrentUserProfile = createSelector(getUserProfileState, fromUserProfile.getCurrentUserProfile);
    static getInProgress = createSelector(getUserProfileState, fromUserProfile.getInProgress);
    static getActionError = createSelector(getUserProfileState, fromUserProfile.getActionError);
}

// Location Selectors
export class LocationSelectors {
    static getAllLocations = createSelector(getLocationState, fromLocation.getEntities);
    static getSelectedLocationId = createSelector(getLocationState, fromLocation.getSelectedEntityId);
    static getLocationsLoading = createSelector(getLocationState, fromLocation.getLoading);
    static getLocationsLoadingError = createSelector(getLocationState, fromLocation.getLoadingError);
    static getSelectedLocation = createSelector(getLocationState, fromLocation.getSelectedEntity);
}

export class LocationEquipmentSelectors {
    static getAllLocationEquipment = createSelector(getLocationEquipmentState, fromLocationEquipment.getEntities);
    static getSelectedLocationEquipmentId = createSelector(getLocationEquipmentState, fromLocationEquipment.getSelectedEntityId);
    static getLocationEquipmentLoading = createSelector(getLocationEquipmentState, fromLocationEquipment.getLoading);
    static getLocationEquipmentLoadingError = createSelector(getLocationEquipmentState, fromLocationEquipment.getLoadingError);
    static getSelectedLocationEquipment = createSelector(getLocationEquipmentState, fromLocationEquipment.getSelectedEntity);
}
