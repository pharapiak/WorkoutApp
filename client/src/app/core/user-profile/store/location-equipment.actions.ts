import { Action } from '@ngrx/store';

import { type } from 'app/util';
import * as appActions from 'app/app.actions';
import { LocationEquipment } from '../location-equipment';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const LocationEquipmentActionTypes = {
    LOAD_ALL: type('[LocationEquipment] Load All'),
    LOAD_ALL_COMPLETE: type('[LocationEquipment] Load All Complete'),
    LOAD_ALL_ERROR: type('[LocationEquipment] Load All Error'),
    // LOAD: type('[LocationEquipment] Load'),
    // LOAD_COMPLETE: type('[LocationEquipment] Load Complete'),
    // LOAD_ERROR: type('[LocationEquipment] Load Error'),
    ADD: type('[LocationEquipment] Add'),
    ADD_COMPLETE: type('[LocationEquipment] Add Complete'),
    ADD_ERROR: type('[LocationEquipment] Add Error'),
    DELETE: type('[LocationEquipment] Delete'),
    DELETE_COMPLETE: type('[LocationEquipment] Delete Complete'),
    DELETE_ERROR: type('[LocationEquipment] Delete Error'),
    UPDATE: type('[LocationEquipment] Update'),
    UPDATE_COMPLETE: type('[LocationEquipment] Update Complete'),
    UPDATE_ERROR: type('[LocationEquipment] Update Error'),
    SELECT: type('[LocationEquipment] Select'),
    SELECT_COMPLETE: type('[LocationEquipment] Select Complete'),
};


/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */

// Load All for a specific location
export class LoadAllAction implements Action {
    type = LocationEquipmentActionTypes.LOAD_ALL;
    constructor(public payload: string) { } // locationId
}
export class LoadAllCompleteAction implements Action {
    type = LocationEquipmentActionTypes.LOAD_ALL_COMPLETE;
    constructor(public payload: LocationEquipment[]) { }
}
export class LoadAllErrorAction implements Action {
    type = LocationEquipmentActionTypes.LOAD_ALL_ERROR;
    constructor(public payload: string) { }
}

// Load
// export class LoadAction implements Action {
//     type = LocationEquipmentActionTypes.LOAD;
//     constructor(public payload: string) { } // locationEquipment id
// }
// export class LoadCompleteAction implements Action {
//     type = LocationEquipmentActionTypes.LOAD_COMPLETE;
//     constructor(public payload: LocationEquipment) { }
// }
// export class LoadErrorAction implements Action {
//     type = LocationEquipmentActionTypes.LOAD_ERROR;
//     constructor(public payload: string) { }
// }
// Add
export class AddAction implements Action {
    type = LocationEquipmentActionTypes.ADD;
    constructor(public payload: LocationEquipment) { }
}
export class AddCompleteAction implements Action {
    type = LocationEquipmentActionTypes.ADD_COMPLETE;
    constructor(public payload: LocationEquipment) { }
}
export class AddErrorAction implements Action {
    type = LocationEquipmentActionTypes.ADD_ERROR;
    constructor(public payload: string) { }
}
// Delete
export class DeleteAction implements Action {
    type = LocationEquipmentActionTypes.DELETE;
    constructor(public payload: string) { }
}
export class DeleteCompleteAction implements Action {
    type = LocationEquipmentActionTypes.DELETE_COMPLETE;
    constructor(public payload: string) { }
}
export class DeleteErrorAction implements Action {
    type = LocationEquipmentActionTypes.DELETE_ERROR;
    constructor(public payload: string) { }
}
// Update
// export class UpdateAction implements Action {
//     type = LocationEquipmentActionTypes.UPDATE;
//     constructor(public payload: LocationEquipment) { }
// }
// export class UpdateCompleteAction implements Action {
//     type = LocationEquipmentActionTypes.UPDATE_COMPLETE;
//     constructor(public payload: LocationEquipment) { }
// }
// export class UpdateErrorAction implements Action {
//     type = LocationEquipmentActionTypes.UPDATE_ERROR;
//     constructor(public payload: string) { }
// }

// Select
export class SelectAction implements Action {
    type = LocationEquipmentActionTypes.SELECT;
    constructor(public payload: string) { }
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type LocationEquipmentActions =
    appActions.LogoutAction
    // | LoadCompleteAction
    // | LoadAction
    // | LoadCompleteAction
    // | LoadErrorAction
    | LoadAllAction
    | LoadAllCompleteAction
    | LoadAllErrorAction
    | AddAction
    | AddCompleteAction
    | AddErrorAction
    | DeleteAction
    | DeleteCompleteAction
    | DeleteErrorAction
    // | UpdateAction
    // | UpdateCompleteAction
    // | UpdateErrorAction
    | SelectAction
    ;
