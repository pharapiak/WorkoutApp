import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';

// import { GoogleBooksService } from '../services/google-books';
// import * as book from '../actions/book';
import * as userActions from 'app/core/user/store/user.actions';
import * as locationEquipmentActions from './location-equipment.actions';
import { LocationEquipmentService } from '../location-equipment.service';

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application. StateUpdates is an observable of the latest state and
 * dispatched action. The `toPayload` helper function returns just
 * the payload of the currently dispatched action, useful in
 * instances where the current state is not necessary.
 *
 * If you are unfamiliar with the operators being used in these examples, please
 * check out the sources below:
 *
 * Official Docs: http://reactivex.io/rxjs/manual/overview.html#categories-of-operators
 * RxJS 5 Operators By Example: https://gist.github.com/btroncone/d6cf141d6f2c00dc6b35
 */

@Injectable()
export class LocationEquipmentEffects {

    @Effect()
    loadAll$: Observable<Action> = this.actions$
        .ofType(locationEquipmentActions.LocationEquipmentActionTypes.LOAD_ALL)
        .map(toPayload)
        .switchMap((locationId) => {
            return this.locationEquipmentService.getAllForLocation(locationId)
                .map(result => {
                    return new locationEquipmentActions.LoadAllCompleteAction(result);
                })
                .catch((error) => of(new locationEquipmentActions.LoadAllErrorAction(error)));
        });

    // @Effect()
    // load$: Observable<Action> = this.actions$
    //     .ofType(locationEquipmentActions.LocationEquipmentActionTypes.LOAD)
    //     .map((action: locationEquipmentActions.LoadAction) => action.payload)
    //     .switchMap((id) => {
    //         return this.locationEquipmentService.get(id)
    //             .map(result => new locationEquipmentActions.LoadCompleteAction(result))
    //             .catch((error) => of(new locationEquipmentActions.LoadErrorAction(error)));
    //     });

    @Effect()
    add$: Observable<Action> = this.actions$
        .ofType(locationEquipmentActions.LocationEquipmentActionTypes.ADD)
        .map(toPayload)
        .mergeMap((data) => {
            return this.locationEquipmentService.create(data)
                .map(result => new locationEquipmentActions.AddCompleteAction(result))
                .catch((error) => of(new locationEquipmentActions.AddErrorAction(error)));
        });

    @Effect()
    delete$: Observable<Action> = this.actions$
        .ofType(locationEquipmentActions.LocationEquipmentActionTypes.DELETE)
        .map(toPayload)
        .mergeMap((data) => {
            return this.locationEquipmentService.delete(data)
                .map(result => new locationEquipmentActions.DeleteCompleteAction(result))
                .catch((error) => of(new locationEquipmentActions.DeleteErrorAction(error)));
        });

    constructor(
        private actions$: Actions,
        private locationEquipmentService: LocationEquipmentService
    ) { }



}
