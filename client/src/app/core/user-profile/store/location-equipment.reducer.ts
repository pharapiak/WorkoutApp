import { createSelector } from 'reselect';

import { AppActionTypes } from "app/app.actions";
import { UserProfile } from "app/core/user-profile/user-profile";
import { UserProfileActionTypes } from "./user-profile.actions";
import { LocationEquipment } from '../location-equipment';
import { LocationEquipmentActionTypes, LocationEquipmentActions } from "./location-equipment.actions";

export interface LocationEquipmentState {
    entities: LocationEquipment[];
    selectedEntityId: string;
    loading: boolean;
    loadingError: string;
};

export const initialLocationEquipmentState: LocationEquipmentState = {
    entities: new Array<LocationEquipment>(),
    selectedEntityId: '0',
    loading: false,
    loadingError: null,
};


export function locationEquipmentReducer(
    state: LocationEquipmentState = initialLocationEquipmentState,
    action: LocationEquipmentActions): LocationEquipmentState {
    switch (action.type) {

        case AppActionTypes.LOGOUT: {
            return Object.assign({}, state, initialLocationEquipmentState);
        }

        case LocationEquipmentActionTypes.LOAD_ALL: {
            return Object.assign({}, state, { loading: true, loadingError: null });
        }

        case LocationEquipmentActionTypes.LOAD_ALL_COMPLETE: {
            return Object.assign({}, state, { entities: action.payload, loading: false, loadingError: null });
        }
        case LocationEquipmentActionTypes.LOAD_ALL_ERROR: {
            return Object.assign({}, state, { loading: false, loadingError: action.payload });
        }
        case LocationEquipmentActionTypes.ADD_COMPLETE: {
            console.log('ADD_COMPLETE');
            return Object.assign({}, state, { entities: [...state.entities, action.payload] });
        }
        case LocationEquipmentActionTypes.DELETE_COMPLETE: {
            return Object.assign({}, state, { entities: state.entities.filter(x => x.id !== action.payload) });
        }
        case LocationEquipmentActionTypes.UPDATE_COMPLETE: {
            break;
        }
        case LocationEquipmentActionTypes.SELECT: {
            return Object.assign({}, state, { selectedEntityId: action.payload });
        }
        default: {
            return state;
        }
    }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */
export const getSelectedEntityId = (state: LocationEquipmentState) => state.selectedEntityId;

export const getEntities = (state: LocationEquipmentState) => state.entities;

export const getLoading = (state: LocationEquipmentState) => state.loading;

export const getLoadingError = (state: LocationEquipmentState) => state.loadingError;

export const getSelectedEntity = (state: LocationEquipmentState) => {
    console.log('getSelectedEntity');
    // console.dir(state);
    return state.entities.find(x => x.id === state.selectedEntityId);
}
