import { Action } from '@ngrx/store';

import { type } from 'app/util';
import * as appActions from 'app/app.actions';
import * as userProfileActions from 'app/core/user-profile/store/user-profile.actions';
import { Location } from '../location';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 * 
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique. 
 */
export const LocationActionTypes = {
    LOAD_ALL: type('[Location] Load All'),
    LOAD_ALL_COMPLETE: type('[Location] Load All Complete'),
    LOAD_ALL_ERROR: type('[Location] Load All Error'),
    ADD: type('[Location] Add'),
    ADD_COMPLETE: type('[Location] Add Complete'),
    ADD_ERROR: type('[Location] Add Error'),
    DELETE: type('[Location] Delete'),
    DELETE_COMPLETE: type('[Location] Delete Complete'),
    DELETE_ERROR: type('[Location] Delete Error'),
    UPDATE: type('[Location] Update'),
    UPDATE_COMPLETE: type('[Location] Update Complete'),
    UPDATE_ERROR: type('[Location] Update Error'),
    SELECT: type('[Location] Select'),
};


/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful 
 * type checking in reducer functions.
 * 
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */

// Load All - these are now loaded with the user profile

// Add
export class AddAction implements Action {
    type = LocationActionTypes.ADD;
    constructor(public payload: Location) { }
}
export class AddCompleteAction implements Action {
    type = LocationActionTypes.ADD_COMPLETE;
    constructor(public payload: Location) { }
}
export class AddErrorAction implements Action {
    type = LocationActionTypes.ADD_ERROR;
    constructor(public payload: string) { }
}
// Delete
export class DeleteAction implements Action {
    type = LocationActionTypes.DELETE;
    constructor(public payload: string) { }
}
export class DeleteCompleteAction implements Action {
    type = LocationActionTypes.DELETE_COMPLETE;
    constructor(public payload: string) { }
}
export class DeleteErrorAction implements Action {
    type = LocationActionTypes.DELETE_ERROR;
    constructor(public payload: string) { }
}
// Update
export class UpdateAction implements Action {
    type = LocationActionTypes.UPDATE;
    constructor(public payload: Location) { }
}
export class UpdateCompleteAction implements Action {
    type = LocationActionTypes.UPDATE_COMPLETE;
    constructor(public payload: Location) { }
}
export class UpdateErrorAction implements Action {
    type = LocationActionTypes.UPDATE_ERROR;
    constructor(public payload: string) { }
}
// Select
export class SelectAction implements Action {
    type = LocationActionTypes.SELECT;
    constructor(public payload: string) { }
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type LocationActions =
    appActions.LogoutAction
    | userProfileActions.LoadCompleteAction
    | AddAction
    | AddCompleteAction
    | AddErrorAction
    | DeleteAction
    | DeleteCompleteAction
    | DeleteErrorAction
    | UpdateAction
    | UpdateCompleteAction
    | UpdateErrorAction
    | SelectAction
    ;
