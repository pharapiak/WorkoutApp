import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

// import { GoogleBooksService } from '../services/google-books';
// import * as book from '../actions/book';
import * as locationActions from './location.actions';
import { LocationService } from '../location.service';
import { AppState } from "app/app.store";

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application. StateUpdates is an observable of the latest state and
 * dispatched action. The `toPayload` helper function returns just
 * the payload of the currently dispatched action, useful in
 * instances where the current state is not necessary.
 *
 * If you are unfamiliar with the operators being used in these examples, please
 * check out the sources below:
 *
 * Official Docs: http://reactivex.io/rxjs/manual/overview.html#categories-of-operators
 * RxJS 5 Operators By Example: https://gist.github.com/btroncone/d6cf141d6f2c00dc6b35
 */

@Injectable()
export class LocationEffects {

    @Effect()
    add$: Observable<Action> = this.actions$
        .ofType(locationActions.LocationActionTypes.ADD)
        // .map((action: locationActions.AddAction) => action.payload)
        .map(toPayload)
        .mergeMap((data) => {
            return this.locationService.create(data)
                .map(result => new locationActions.AddCompleteAction(result))
                .catch((error) => of(new locationActions.AddErrorAction(error)));
        });

    @Effect()
    delete$: Observable<Action> = this.actions$
        .ofType(locationActions.LocationActionTypes.DELETE)
        .map(toPayload)
        .mergeMap((data) => {
            return this.locationService.delete(data)
                .map(result => new locationActions.DeleteCompleteAction(result))
                .catch((error) => of(new locationActions.DeleteErrorAction(error)));
        });


    constructor(
        private store: Store<AppState>,
        private actions$: Actions,
        private locationService: LocationService
    ) {
    }

}
