import { createSelector } from 'reselect';

import { AppActionTypes } from "app/app.actions";
import { UserProfile } from "app/core/user-profile/user-profile";
import { UserProfileActionTypes } from "./user-profile.actions";
import { Location } from '../location';
import { LocationActionTypes, LocationActions } from "./location.actions";
import { LocationService } from "app/core/user-profile/location.service";

export interface LocationState {
    entities: Location[];
    selectedEntityId: string;
    loading: boolean;
    loadingError: string;
};

export const initialLocationState: LocationState = {
    entities: new Array<Location>(),
    selectedEntityId: '0',
    loading: false,
    loadingError: null,
};


export function locationReducer(state: LocationState = initialLocationState, action: LocationActions): LocationState {
    switch (action.type) {

        case AppActionTypes.LOGOUT: {
            return Object.assign({}, state, initialLocationState);
        }

        // grab the load complete for UserProfile, since it comes in loaded with a list of Locations
        case UserProfileActionTypes.LOAD_COMPLETE: {
            return Object.assign({}, state, { entities: (action.payload as UserProfile).locations, loading: false, loadingError: null });
        }

        case LocationActionTypes.ADD_COMPLETE: {
            console.log('ADD_COMPLETE');
            return Object.assign({}, state, { entities: [...state.entities, action.payload] });
        }
        case LocationActionTypes.DELETE_COMPLETE: {
            return Object.assign({}, state, { entities: state.entities.filter(x => x.id !== action.payload) });
        }
        case LocationActionTypes.UPDATE_COMPLETE: {
            break;
        }
        case LocationActionTypes.SELECT: {
            return Object.assign({}, state, { selectedEntityId: action.payload });
        }
        default: {
            return state;
        }
    }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */
export const getSelectedEntityId = (state: LocationState) => state.selectedEntityId;

export const getEntities = (state: LocationState) => state.entities;

export const getLoading = (state: LocationState) => state.loading;

export const getLoadingError = (state: LocationState) => state.loadingError;

export const getSelectedEntity = (state: LocationState) => {
    console.log('getSelectedEntity');
    // console.dir(state);
    if (state.entities == null) {
        return null;
    }
    return state.entities.find(x => x.id === state.selectedEntityId);
}
