import { Action } from '@ngrx/store';

import { type } from 'app/util';
import * as appActions from "app/app.actions";
import { UserProfile } from '../user-profile';
import { User } from "app/core/user/user";

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 * 
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique. 
 */
export const UserProfileActionTypes = {
    LOAD: type('[UserProfile] Load'),
    LOAD_COMPLETE: type('[UserProfile] Load Complete'),
    LOAD_ERROR: type('[UserProfile] Load Error'),

    UPDATE: type('[UserProfile] Update'),
    UPDATE_COMPLETE: type('[UserProfile] Update Complete'),
    UPDATE_ERROR: type('[UserProfile] Update Error'),

    DELETE: type('[UserProfile] Delete'),
    DELETE_COMPLETE: type('[UserProfile] Delete Complete'),
    DELETE_ERROR: type('[UserProfile] Delete Error'),
};


/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful 
 * type checking in reducer functions.
 * 
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */

// Load
export class LoadAction implements Action {
    type = UserProfileActionTypes.LOAD;
    constructor(public payload: User) { }
}
export class LoadCompleteAction implements Action {
    type = UserProfileActionTypes.LOAD_COMPLETE;
    constructor(public payload: UserProfile) { }
}
export class LoadErrorAction implements Action {
    type = UserProfileActionTypes.LOAD_ERROR;
    constructor(public payload: string) { }
}

// Update
export class UpdateAction implements Action {
    type = UserProfileActionTypes.UPDATE;
    constructor(public payload: UserProfile) { }
}
export class UpdateCompleteAction implements Action {
    type = UserProfileActionTypes.UPDATE_COMPLETE;
    constructor(public payload: UserProfile) { }
}
export class UpdateErrorAction implements Action {
    type = UserProfileActionTypes.UPDATE_ERROR;
    constructor(public payload: string) { }
}

// Delete
export class DeleteAction implements Action {
    type = UserProfileActionTypes.DELETE;
    constructor(public payload: string) { }
}
export class DeleteCompleteAction implements Action {
    type = UserProfileActionTypes.DELETE_COMPLETE;
    constructor(public payload: string) { }
}
export class DeleteErrorAction implements Action {
    type = UserProfileActionTypes.DELETE_ERROR;
    constructor(public payload: string) { }
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type UserProfileActions =
    appActions.LogoutAction
    | LoadAction
    | LoadCompleteAction
    | LoadErrorAction
    | UpdateAction
    | UpdateCompleteAction
    | UpdateErrorAction
    | DeleteAction
    | DeleteCompleteAction
    | DeleteErrorAction
    ;
