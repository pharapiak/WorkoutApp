import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import * as appActions from 'app/app.actions';
import * as userProfileActions from './user-profile.actions';
import { UserProfileService } from '../user-profile.service';
import { AppState } from "app/app.store";
import { User } from "app/core/user/user";
import { UserProfile } from "app/core/user-profile/user-profile";
import { LocationService } from "app/core/user-profile/location.service";
import { SelectAction } from "app/core/user-profile/store/location.actions";

/**
 * Effects offer a way to isolate and easily test side-effects within your
 * application. StateUpdates is an observable of the latest state and
 * dispatched action. The `toPayload` helper function returns just
 * the payload of the currently dispatched action, useful in
 * instances where the current state is not necessary.
 *
 * If you are unfamiliar with the operators being used in these examples, please
 * check out the sources below:
 *
 * Official Docs: http://reactivex.io/rxjs/manual/overview.html#categories-of-operators
 * RxJS 5 Operators By Example: https://gist.github.com/btroncone/d6cf141d6f2c00dc6b35
 */

@Injectable()
export class UserProfileEffects {

    // when a user logs in, if they have an exerciser role, request the loading of their profile
    // this could be done elsewhere, but there are many clients that will need the profile, so I was
    // looking for a central, pro-active place to do it
    @Effect({ dispatch: false })
    loginComplete$ = this.actions$
        .ofType(appActions.AppActionTypes.LOGIN_COMPLETE)
        .map((action: appActions.LoginCompleteAction) => action.payload)
        // use '.do' here instead of map, since we don't want to return an extra action, we're only listening
        .do((user: User) => {
            console.log("userprofile effect on LOGIN_COMPLETE start")
            if (user.isExerciser()) {
                // if the user is  an exerciser we'll always need their profile, so
                // dispatch a request to load it
                this.store.dispatch(new userProfileActions.LoadAction(user));

                // also try and get the last location that the user selected
                const lastLocationId = LocationService.getMostRecentLocationId(user.id);
                if (lastLocationId) {
                    this.store.dispatch(new SelectAction(lastLocationId));
                }
            }
            console.log("userprofile effect on LOGIN_COMPLETE end");
        });

    @Effect()
    load$: Observable<Action> = this.actions$
        .ofType(userProfileActions.UserProfileActionTypes.LOAD)
        .map((action: userProfileActions.LoadAction) => action.payload)
        .switchMap((user: User, index: number): Observable<UserProfile> => {
            console.log(`UserProfile effect on User Profile LOAD start, got number ${index}`)
            return this.userProfileService.getOrCreateForUserId(user.id);
        })
        .map((userProfile: UserProfile) => {
            console.log("UserProfile effect on User Profile LOAD end")
            return new userProfileActions.LoadCompleteAction(userProfile)
        })
        .catch((error: string) => {
            console.log("UserProfile effect on User Profile LOAD error");
            // of(new userProfileActions.LoadErrorAction(error));
            return Observable.of(new userProfileActions.LoadErrorAction(error));
        });

    @Effect()
    update$: Observable<Action> = this.actions$
        .ofType(userProfileActions.UserProfileActionTypes.UPDATE)
        .map((action: userProfileActions.UpdateAction) => action.payload)
        .mergeMap((userProfile) => {
            return this.userProfileService.save(userProfile)
                .map(result => new userProfileActions.UpdateCompleteAction(result))
                .catch((error) => of(new userProfileActions.UpdateErrorAction(error)));
        });

    @Effect()
    delete$: Observable<Action> = this.actions$
        .ofType(userProfileActions.UserProfileActionTypes.DELETE)
        .map((action: userProfileActions.DeleteAction) => action.payload)
        .mergeMap((data) => {
            return this.userProfileService.delete(data)
                .map(result => new userProfileActions.DeleteCompleteAction(result))
                .catch((error) => of(new userProfileActions.DeleteErrorAction(error)));
        });

    constructor(
        private store: Store<AppState>,
        private actions$: Actions,
        private userProfileService: UserProfileService
    ) { }

}
