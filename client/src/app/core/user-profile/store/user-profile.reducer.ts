import { createSelector } from 'reselect';

import * as appActions from 'app/app.actions';
import { UserProfile } from '../user-profile';
import * as userProfileActions from './user-profile.actions';

export interface UserProfileState {
    currentUserProfile: UserProfile;
    inProgress: boolean;
    actionError: string;
};

export const initialUserProfileState: UserProfileState = {
    currentUserProfile: null,
    inProgress: false,
    actionError: null,
};

export function userProfileReducer(state: UserProfileState = initialUserProfileState, action: userProfileActions.UserProfileActions): UserProfileState {
    switch (action.type) {

        case appActions.AppActionTypes.LOGOUT: {
            console.log('LOGOUT');
            return Object.assign({}, state, initialUserProfileState);
        }

        case userProfileActions.UserProfileActionTypes.LOAD: {
            console.log('userProfileReducer LOAD');
            return Object.assign({}, state, { inProgress: true });
        }
        case userProfileActions.UserProfileActionTypes.LOAD_COMPLETE: {
            console.log('userProfileReducer LOAD_COMPLETE');
            return Object.assign({}, state, { currentUserProfile: action.payload, inProgress: false, actionError: null });
        }
        case userProfileActions.UserProfileActionTypes.LOAD_ERROR: {
            console.log('userProfileReducer LOAD_ERROR');
            return Object.assign({}, state, { inProgress: false, actionError: action.payload });
        }
        // case userActions.ActionTypes.DELETE_COMPLETE: {
        //     return Object.assign({}, state, { entities: state.currentUser.filter(x => x.id !== action.payload) });
        // }
        case userProfileActions.UserProfileActionTypes.UPDATE_COMPLETE: {
            break;
        }
        default: {
            return state;
        }
    }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */
export const getCurrentUserProfile = (state: UserProfileState) => state.currentUserProfile;

export const getInProgress = (state: UserProfileState) => state.inProgress;

export const getActionError = (state: UserProfileState) => state.actionError;

