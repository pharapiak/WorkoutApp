import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from "rxjs/Subject";
import 'rxjs/add/operator/mergeMap'; // huh? This import is needed for  ng test find the flatMap operator below

import { UserProfile } from './user-profile'
import { LocationService } from "./location.service";
import { Location } from './location';
import { Gender } from "app/core/user-profile/gender";

@Injectable()
export class UserProfileService {


  private BASE_URL = 'http://localhost:3000/userProfiles';

  // final implementation can lose the injected LocationService 
  constructor(private http: Http, private locationService: LocationService) { }

  // this will find the user profile for a given user id (creating it if necessary), 
  // along with the workout locations for that profile 
  // (admin - only users don't have a profile)
  public getOrCreateForUserId(userId: string): Observable<UserProfile> {

    //get the profile and the locations for that profile (_embed tells json-server to join the child records)
    let url = `${this.BASE_URL}/?userId=${userId}&_embed=locations`;
    console.log(url);

    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .flatMap((result): Observable<UserProfile> => {
        let profileList = this.parseUserProfileArray(result);

        if (profileList.length > 0) {
          if (profileList.length > 1) {
            // hmmm... should only ever have a single profile for a user id
            console.error(`Found {profileList.length} profiles for user id {userId}`)
          }
          return Observable.of(profileList[0]);
        } else {
          // TODO move this functionality to the server; that's where we should auto-create the profile
          return this.createDefaultProfileForUser(userId);
        }
      }
      );
  }

  public delete(id: string): Observable<string> {
    let url = `${this.BASE_URL}/${id}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .delete(url)
      .map(result => { return id });
  }

  public save(userProfile: UserProfile): Observable<UserProfile> {
    let url = `${this.BASE_URL}/${userProfile.id}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .put(url, userProfile)
      .map(result => result.json());
  }

  private handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    throw (errMsg);
    // return Observable.throw(errMsg);
  }

  private parseUserProfileArray(response: Response): Array<UserProfile> {
    let result = response.json().map(element => {
      let profile: UserProfile = Object.assign(new UserProfile(), element);
      profile.gender = Number(element.gender); //convert from string to Gender enum, which is really a const number
      // profile.gender = Gender[element.gender]; //convert from string to Gender enum 
      profile.locations = element.locations.map(loc => Object.assign(new Location(), loc));
      return profile;
    });
    return result;
  }



  // TODO move this functionality to the server  
  private createDefaultProfileForUser(userId: string): Observable<UserProfile> {
    console.log('auto-create UserProfile');

    let userProfile = new UserProfile();
    userProfile.userId = userId;
    let url = `${this.BASE_URL}`;
    return this.http
      // .post(url, user, { headers: this.stateService.headers })
      .post(url, userProfile)
      .flatMap(res => {
        console.log('auto-create UserProfile, got response from POST');
        let newProfile: UserProfile = res.json() || {};
        // fire off a request to create a default location.  
        // Can't do this in parallel, cuz we need the profile's id from the first create request
        let defaultLocation = new Location();
        defaultLocation.userProfileId = newProfile.id;
        return this.locationService.create(defaultLocation)
          .map(location => {
            newProfile.locations = [location];
            return newProfile;
          }
          );
      }
      );
  }


}
