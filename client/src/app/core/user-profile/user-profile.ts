
import { ModelBase } from 'app/shared/model/model'
import { Location } from './location'
import { Gender } from './gender'

export class UserProfile extends ModelBase {
    public userId: string; // int id of the user that owns this profile
    public gender: Gender;
    public locations: Location[];
}