import { createSelector } from 'reselect';
import { ActionReducer } from '@ngrx/store';
import { compose } from '@ngrx/core';
import { storeFreeze } from 'ngrx-store-freeze';
import { combineReducers } from '@ngrx/store';

import { environment } from '../../../../environments/environment';

export { UserActions } from './user.actions'

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single import name.
 */
import * as fromUser from './user.reducer';
import { AppState } from "app/app.store";

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface UserModuleState {
    user: fromUser.UserState;
}

export const initialUserModule: UserModuleState = {
    user: fromUser.initialUserState,
};

/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */
const reducers = {
    user: fromUser.userReducer,
};

// set up the production vs dev reducers (dev uses storeFreeze to make sure that our reducers are not mutating state)
const developmentReducer: ActionReducer<UserModuleState> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<UserModuleState> = combineReducers(reducers);

export function userModule(state: UserModuleState = initialUserModule, action: any) {
    if (environment.production) {
        return productionReducer(state, action);
    } else {
        return developmentReducer(state, action);
    }
}

/***************************
 * Selectors
 **************************/

const getUserModuleState = (state: AppState) => state.userModule;
const getUserState = (state: AppState) => getUserModuleState(state).user;

// User Selectors
export class UserSelectors {
    static getCurrentUser = createSelector(getUserState, fromUser.getCurrentUser);
    static getIsUserLoggedIn = createSelector(getUserState, fromUser.getIsUserLoggedIn);
    static getInProgress = createSelector(getUserState, fromUser.getInProgress);
    static getActionError = createSelector(getUserState, fromUser.getActionError);
    static getNewUserRegistered = createSelector(getUserState, fromUser.getNewUserRegistered);
}

