import { Action } from '@ngrx/store';

import { type } from 'app/util';
import { User } from '../user';
import * as appActions from "app/app.actions";

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 * 
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique. 
 */
export const UserActionTypes = {
    LOGIN_ATTEMPT: type('[User] Login Attempt'),
    // LOGIN_COMPLETE: type('[User] Login Complete')  // now in app.actions
    LOGIN_ERROR: type('[User] Login Error'),

    REGISTER_ATTEMPT: type('[User] Register'),
    REGISTER_COMPLETE: type('[User] Register Complete'),
    REGISTER_ERROR: type('[User] Register Error'),

    DELETE: type('[User] Delete'),
    DELETE_COMPLETE: type('[User] Delete Complete'),
    DELETE_ERROR: type('[User] Delete Error'),

    UPDATE: type('[User] Update'),
    UPDATE_COMPLETE: type('[User] Update Complete'),
    UPDATE_ERROR: type('[User] Update Error'),
};


/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful 
 * type checking in reducer functions.
 * 
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */

export class LoginCredentials {
    constructor(public username: string, public password: string) { }
}

// Login
export class LoginAttemptAction implements Action {
    type = UserActionTypes.LOGIN_ATTEMPT;
    constructor(public payload: LoginCredentials) { }
}
// export class LoginCompleteAction implements Action {
//     type = ActionTypes.LOGIN_COMPLETE;
//     constructor(public payload: User) { }
// }
export class LoginErrorAction implements Action {
    type = UserActionTypes.LOGIN_ERROR;
    constructor(public payload: string) { }
}

// User registration
export class RegisterAction implements Action {
    type = UserActionTypes.REGISTER_ATTEMPT;
    constructor(public payload: User) { }
}
export class RegisterCompleteAction implements Action {
    type = UserActionTypes.REGISTER_COMPLETE;
    // no use for this payload, but all of these ctors need a payload in the ctor
    // otherwise the union of the actions doesn't necessarily have one, 
    // and the compiler complains
    constructor(public payload: User) { }
}
export class RegisterErrorAction implements Action {
    type = UserActionTypes.REGISTER_ERROR;
    constructor(public payload: string) { }
}

// Delete
export class DeleteAction implements Action {
    type = UserActionTypes.DELETE;
    constructor(public payload: string) { }
}
export class DeleteCompleteAction implements Action {
    type = UserActionTypes.DELETE_COMPLETE;
    constructor(public payload: string) { }
}
export class DeleteErrorAction implements Action {
    type = UserActionTypes.DELETE_ERROR;
    constructor(public payload: string) { }
}
// Update
export class UpdateAction implements Action {
    type = UserActionTypes.UPDATE;
    constructor(public payload: User) { }
}
export class UpdateCompleteAction implements Action {
    type = UserActionTypes.UPDATE_COMPLETE;
    constructor(public payload: User) { }
}
export class UpdateErrorAction implements Action {
    type = UserActionTypes.UPDATE_ERROR;
    constructor(public payload: string) { }
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type UserActions =
    appActions.LogoutAction
    | LoginAttemptAction
    | appActions.LoginCompleteAction
    | LoginErrorAction
    | RegisterAction
    | RegisterCompleteAction
    | RegisterErrorAction
    | DeleteAction
    | DeleteCompleteAction
    | DeleteErrorAction
    | UpdateAction
    | UpdateCompleteAction
    | UpdateErrorAction
    ;
