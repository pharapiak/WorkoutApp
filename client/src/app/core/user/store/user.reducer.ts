import { createSelector } from 'reselect';

import { AuthService } from "../../auth/auth.service";
import { User } from '../user';
import * as appActions from 'app/app.actions';
import * as userActions from './user.actions';

export interface UserState {
    currentUser: User;
    inProgress: boolean;
    actionError: string;
    newUserRegistered: boolean;
};

export const initialUserState: UserState = {
    currentUser: null,
    inProgress: false,
    actionError: null,
    newUserRegistered: false,
};


export function userReducer(state: UserState = initialUserState, action: userActions.UserActions): UserState {
    switch (action.type) {

        case appActions.AppActionTypes.LOGOUT: {
            console.log('LOGOUT');
            return Object.assign({}, state, initialUserState);
        }

        case userActions.UserActionTypes.LOGIN_ATTEMPT: {
            console.log('userReducer LOGIN_ATTEMPT');
            return Object.assign({}, state, { inProgress: true, actionError: null, currentUser: null });
        }
        case appActions.AppActionTypes.LOGIN_COMPLETE: {
            console.log('userReducer LOGIN_COMPLETE');
            return Object.assign({}, state, { currentUser: action.payload, inProgress: false, actionError: null });
        }
        case userActions.UserActionTypes.LOGIN_ERROR: {
            console.log('userReducer LOGIN_ERROR');
            return Object.assign({}, state, { inProgress: false, actionError: action.payload });
        }

        case userActions.UserActionTypes.REGISTER_ATTEMPT: {
            console.log('userReducer REGISTER_ATTEMPT');
            return Object.assign({}, state, { inProgress: true });
        }
        case userActions.UserActionTypes.REGISTER_COMPLETE: {
            console.log('userReducer REGISTER_COMPLETE');
            return Object.assign({}, state, { newUserRegistered: true, inProgress: false, actionError: null });
        }
        case userActions.UserActionTypes.REGISTER_ERROR: {
            console.log('userReducer REGISTER_ERROR');
            return Object.assign({}, state, { inProgress: false, actionError: action.payload });
        }
        // case userActions.ActionTypes.DELETE_COMPLETE: {
        //     return Object.assign({}, state, { entities: state.currentUser.filter(x => x.id !== action.payload) });
        // }
        case userActions.UserActionTypes.UPDATE_COMPLETE: {
            break;
        }
        default: {
            return state;
        }
    }
}

/**
 * Because the data structure is defined within the reducer it is optimal to
 * locate our selector functions at this level. If store is to be thought of
 * as a database, and reducers the tables, selectors can be considered the
 * queries into said database. Remember to keep your selectors small and
 * focused so they can be combined and composed to fit each particular
 * use-case.
 */
export const getCurrentUser = (state: UserState) => state.currentUser;

export const getIsUserLoggedIn = (state: UserState) => (state.currentUser != null);

export const getInProgress = (state: UserState) => state.inProgress;

export const getActionError = (state: UserState) => state.actionError;

export const getNewUserRegistered = (state: UserState) => state.newUserRegistered;

