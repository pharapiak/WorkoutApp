

// This is simple-minded start on a security model.
// logic elsewhere will enable or hide features based on the roles that a user carries
export enum UserRole {
    Exerciser = 1,
    Administrator = 2
}