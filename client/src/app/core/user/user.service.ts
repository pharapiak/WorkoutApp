import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { User } from './user'

@Injectable()
export class UserService {

  private static parseUserArray(response: Response): Array<User> {
    let result = response.json().map(element => UserService.parseUser(element));
    return result;
  }

  public static parseUser(jsonUser: any): User {
    let user: User = Object.assign(new User(), jsonUser);
    // convert the string ids of roles to UserRole enum
    user.roles = jsonUser.roles.map(roleId => Number(roleId));
    return user;
  }

  private BASE_URL = 'http://localhost:3000/users';

  constructor(private http: Http) { }

  public getByUsername(username: string): Observable<User> {
    let url = `${this.BASE_URL}?username=${username.toLowerCase()}`;
    console.log(url);
    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .map(result => {
        let userList = UserService.parseUserArray(result);
        return userList.length == 0 ? null : userList[0];
      }
      )
      .delay(500); // TODO get rid of this, just to demonstrate "Logging in" hourglass
  }

  public getAll(): Observable<Array<User>> {
    let url = `${this.BASE_URL}`;
    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .map(result => {
        return UserService.parseUserArray(result);
      }
      );
  }

  // this is for the system id of the user  
  public get(userId: string): Observable<User> {
    let url = `${this.BASE_URL}/${userId}`;
    return this.http
      // .get(url, { headers: this.stateService.headers })
      .get(url)
      .map(result => result.json());
  }

  public create(user: User): Observable<User> {
    console.log('create user');
    console.dir(user);
    let url = `${this.BASE_URL}`;
    return this.http
      // .post(url, user, { headers: this.stateService.headers })
      .post(url, user)
      .map(result => {
        let userList = UserService.parseUserArray(result);
        return userList.length == 0 ? null : userList[0];
      }
      )
      .delay(500); //TODO get rid of this, just to demonstrate "Registering" hourglass
  }

  public delete(userId: string): Observable<string> {
    let url = `${this.BASE_URL}/${userId}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .delete(url)
      .map(result => { return userId });
  }

  public save(user: User) {
    let url = `${this.BASE_URL}/${user.id}`;
    return this.http
      // .delete(url, { headers: this.stateService.headers })
      .put(url, user)
      .map(result => result.json);
  }

  private handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    throw (errMsg);
    // return Observable.throw(errMsg);
  }
}
