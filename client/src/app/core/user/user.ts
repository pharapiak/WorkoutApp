import { ModelBase } from 'app/shared/model/model'
import { UserRole } from "./user-role";

export class User extends ModelBase {
    public firstName: string;

    public lastName: string;

    public email: string;

    public username: string; // email address or username

    public password: string; // TODO this should be one-way hash of password

    public picture: string; // URL 

    public roles: UserRole[];

    constructor() {
        super();
        // set up all users as exercisers
        this.roles = [UserRole.Exerciser];
    }

    public hasRole(role: UserRole): boolean {
        return this.roles.indexOf(role) >= 0;
    }

    public isExerciser(): boolean {
        return this.hasRole(UserRole.Exerciser);
    }
}