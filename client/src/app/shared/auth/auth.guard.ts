import { Injectable } from '@angular/core';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';
import { AuthService } from 'app/core/auth/auth.service';
import { Store } from "@ngrx/store";
import { AppState } from "app/app.store";
import { UserSelectors } from "app/core/user/store";

// This class is registered as the security guard for routes at the application level.
// when incoming route requests are being evaluated, they are passed to the canActivate method
// TODO at some point, need to differentiate between regular users and admin users, and the pages they can access
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private store: Store<AppState>, private authService: AuthService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let url: string = state.url;

        return this.checkLogin(url);
    }

    checkLogin(url: string): boolean {
        if (this.authService.isLoggedIn) {
            return true;
        }

        // Store the attempted URL for redirecting
        this.authService.redirectUrl = url;

        // Navigate to the login page with extras
        this.router.navigate(['/auth/login']);
        return false;
    }
}
