import { Component, OnInit } from '@angular/core';
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";

import { User } from "app/core/user/user";
import { AuthService } from "app/core/auth/auth.service";
import { AppState } from "app/app.store";
import { UserSelectors } from "app/core/user/store";

@Component({
  selector: 'chat-widget',
  templateUrl: './chat-widget.component.html',
})
export class ChatWidgetComponent implements OnInit {

  public currentUser$: Observable<User>;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.currentUser$ = this.store.select(UserSelectors.getCurrentUser);
  }

}
