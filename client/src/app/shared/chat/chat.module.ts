import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChatWidgetComponent } from './chat-widget/chat-widget.component';
import { SmartadminWidgetsModule } from "../widgets/smartadmin-widgets.module";
import { BsDropdownModule } from "ngx-bootstrap";
import { ChatService } from "./chat.service";
import { ChatComponent } from './chat/chat.component';
import { ChatUsersComponent } from './chat/chat-users.component';
import { ChatBodyComponent } from './chat/chat-body.component';
import { ChatFormComponent } from './chat/chat-form.component';
import { FormsModule } from "@angular/forms";
import { UtilsModule } from "../utils/utils.module";
import { UserCoreModule } from "app/core/user/user-core.module";
import { AsideChatComponent } from './aside-chat/aside-chat.component';
import { AsideChatUserComponent } from './aside-chat-user/aside-chat-user.component';
import { PopoverModule } from "ngx-popover";
@NgModule({
  imports: [
    PopoverModule,
    CommonModule, FormsModule, BsDropdownModule, UtilsModule, UserCoreModule, SmartadminWidgetsModule],
  declarations: [ChatWidgetComponent, ChatComponent, ChatUsersComponent, ChatBodyComponent, ChatFormComponent, AsideChatComponent, AsideChatUserComponent],
  exports: [ChatWidgetComponent, AsideChatComponent, AsideChatUserComponent],
  providers: [ChatService]

})
export class ChatModule { }
