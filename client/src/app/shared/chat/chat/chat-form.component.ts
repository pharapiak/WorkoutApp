import { Component, OnInit, Input } from '@angular/core';

import { ChatService } from "../chat.service";
import { AuthService } from "app/core/auth/auth.service";
import { User } from "app/core/user/user";

@Component({
  selector: 'chat-form',
  templateUrl: './chat-form.component.html',
})
export class ChatFormComponent implements OnInit {

  public message: string = '';

  @Input()
  public currentUser: User;

  public enterToSend: boolean = false;

  constructor(private chatService: ChatService, private authService: AuthService) { }


  ngOnInit() {
    this.chatService.messageToSubject.subscribe((user) => {
      this.message += (user.username + ', ');
    });
  }

  sendMessage() {
    if (this.message.trim() == '') return;
    this.chatService.sendMessage({
      body: this.message,
      user: this.currentUser,
      date: new Date()
    });
    this.message = ''

  }

  sendMessageEnter() {
    if (this.enterToSend) {
      this.sendMessage()
    }
  }

}
