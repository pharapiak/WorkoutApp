import { Component, OnInit, Input } from '@angular/core';
import { ChatService } from "../chat.service";
import { User } from 'app/core/user/user';

@Component({
  selector: 'chat',
  templateUrl: './chat.component.html',
  styles: []
})
export class ChatComponent implements OnInit {

  constructor(private chatService: ChatService) { }

  @Input()
  public currentUser: User;

  public users = [];
  public messages = [];

  ngOnInit() {
    this.chatService.getChatState().subscribe((state) => {
      this.users = state.users;
      this.messages = state.messages;
    })
  }

}
