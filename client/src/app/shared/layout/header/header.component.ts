import { Component, ChangeDetectionStrategy, OnInit, OnChanges, SimpleChange } from '@angular/core';
import { Router } from "@angular/router";
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { AppState } from 'app/app.store';

import { Location } from 'app/core/user-profile/location';
import { LocationSelectors } from 'app/core/user-profile/store/index';
import * as locationActions from 'app/core/user-profile/store/location.actions';
import { UserSelectors } from "app/core/user/store";
import { User } from "app/core/user/user";
import { LocationService } from "app/core/user-profile/location.service";
import { AuthService } from "app/core/auth/auth.service";

declare var $: any;

@Component({
  selector: 'sa-header',
  templateUrl: 'header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {

  private locationsLoading$: Observable<boolean>;
  private locations$: Observable<Location[]>;
  private currentLocation$: Observable<Location>;

  constructor(private router: Router, private store: Store<AppState>, private authService: AuthService) {
  }

  ngOnInit() {
    console.log("HeaderComponent ngOnInit ");
    this.locations$ = this.store.select(LocationSelectors.getAllLocations);
    this.currentLocation$ = this.store.select(LocationSelectors.getSelectedLocation);
    this.locationsLoading$ = this.store.select(LocationSelectors.getLocationsLoading);
  }

  onLocationChanged(locationId: string) {
    this.store.dispatch(new locationActions.SelectAction(locationId));
    const currentUser = this.authService.currentUser;
    if (currentUser) {
      LocationService.setMostRecentLocationId(currentUser.id, locationId);
    }
  }

}
