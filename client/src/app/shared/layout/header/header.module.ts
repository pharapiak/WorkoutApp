import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BsDropdownModule } from "ngx-bootstrap";

import { FullScreenComponent } from "./full-screen/full-screen.component";
import { CollapseMenuComponent } from "./collapse-menu/collapse-menu.component";

import { ActivitiesComponent } from "./activities/activities.component";
import { ActivitiesMessageComponent } from "./activities/activities-message/activities-message.component";
import { ActivitiesNotificationComponent } from "./activities/activities-notification/activities-notification.component";
import { ActivitiesTaskComponent } from "./activities/activities-task/activities-task.component";
import { HeaderComponent } from "./header.component";

import { UtilsModule } from "../../utils/utils.module";
import { I18nModule } from "../../i18n/i18n.module";
import { SpeechButtonComponent } from './speech-button/speech-button.component';
import { FormsModule } from "@angular/forms";
import { PopoverModule } from "ngx-popover";
import { LoadingModule } from '../../loading/loading.module';
import { LoginInfoModule } from "app/shared/login-info/login-info.module";
import { LogoutModule } from "app/shared/logout/logout.module";
import { LocationSelectorModule } from "app/shared/location-selector/location-selector.module";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        BsDropdownModule,

        UtilsModule,
        I18nModule,
        PopoverModule,
        LoadingModule,
        LoginInfoModule,
        LogoutModule,
        LocationSelectorModule,
    ],
    declarations: [
        ActivitiesMessageComponent,
        ActivitiesNotificationComponent,
        ActivitiesTaskComponent,
        FullScreenComponent,
        CollapseMenuComponent,
        ActivitiesComponent,
        HeaderComponent,
        SpeechButtonComponent,
    ],
    exports: [
        HeaderComponent,
    ]
})
export class HeaderModule { }
