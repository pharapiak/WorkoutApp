import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Location } from 'app/core/user-profile/location';

import { LocationService } from 'app/core/user-profile/location.service';

@Component({
  selector: 'app-location-selector',
  templateUrl: 'location-selector.component.html'
})
export class LocationSelectorComponent {

  @Input()
  locations: Location[];

  @Input()
  currentLocation: Location;

  @Output() onLocationChanged: EventEmitter<string> = new EventEmitter();

  constructor(private locationsService: LocationService) {
  }

  public setLocation(locationId: string): void {
    // toString cuz we're getting an int from the dropdown list
    this.onLocationChanged.emit(locationId.toString());
  }
}
