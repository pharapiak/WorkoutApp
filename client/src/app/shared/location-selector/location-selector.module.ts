import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap';

import { I18nModule } from "../i18n/i18n.module";
import { LocationSelectorComponent } from "./location-selector.component";


@NgModule({
  imports: [
    CommonModule,
    BsDropdownModule,
    I18nModule,
  ],
  declarations: [
    LocationSelectorComponent,
  ],
  exports: [
    LocationSelectorComponent,
  ]
})
export class LocationSelectorModule { }
