import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { Store } from "@ngrx/store";

import { AppState } from "app/app.store";
import { User } from "app/core/user/user";
import { UserSelectors } from 'app/core/user/store/index';

@Component({

  selector: 'app-login-info',
  templateUrl: 'login-info.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginInfoComponent implements OnInit {

  public user$: Observable<User>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.user$ = this.store.select(UserSelectors.getCurrentUser);
    console.log("LoginInfoComponent initialized");
  }

}
