import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";

import { LoginInfoComponent } from "./login-info.component";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
    ],
    declarations: [LoginInfoComponent],
    exports: [LoginInfoComponent],
})
export class LoginInfoModule { }
