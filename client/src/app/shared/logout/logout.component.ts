import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";

import { AppState } from "app/app.store";
import { AuthService } from "app/core/auth/auth.service";

declare var $: any;

@Component({
  selector: 'sa-logout',
  template: `
<div id="logout" (click)="showPopup()" class="btn-header transparent pull-right">
        <span> 
         <a routerlink="/auth/login" title="Sign Out" data-action="userLogout" >
         <i class="fa fa-sign-out"></i></a> 
        </span>
    </div>
  `,
  styles: []
})
export class LogoutComponent implements OnInit {

  constructor(private store: Store<AppState>, private router: Router, private authService: AuthService) { }

  showPopup() {
    $.SmartMessageBox({
      title: "<i class='fa fa-sign-out txt-color-orangeDark'></i> Logout <span class='txt-color-orangeDark'><strong>" + $('#show-shortcut').text() + "</strong></span> ?",
      content: "",
      buttons: '[No][Yes]'

    }, (ButtonPressed) => {
      if (ButtonPressed == "Yes") {
        this.logout()
      }
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/auth/login'])
  }

  ngOnInit() {

  }



}
