
// base class for model classes stored to the database
export abstract class ModelBase {
    id: string;
    version: number;
}
