import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import {
  // ModalModule, 
  // ButtonsModule, 
  TooltipModule,
  // ProgressbarModule, 
  AlertModule,
  // TabsModule,
  // AccordionModule, 
  // CarouselModule,
  ModalModule,
  BsDropdownModule,
} from 'ngx-bootstrap';

import { PopoverModule } from "ngx-popover";

import { JsonApiService } from './api'
import { AuthGuard } from "./auth/auth.guard";
import { LayoutService } from './layout/layout.service'
import { LayoutModule } from './layout/layout.module'
import { LoadingModule } from './loading/loading.module';
import { I18nModule } from "./i18n/i18n.module";
import { LoginInfoModule } from "./login-info/login-info.module";
import { LogoutModule } from "./logout/logout.module";
import { UserCoreModule } from "app/core/user/user-core.module";

import { UtilsModule } from "./utils/utils.module";
import { SmartadminFormsModule } from "./forms/smartadmin-forms.module";
import { SmartProgressbarModule } from './ui/smart-progressbar/smart-progressbar.module';
import { LocationSelectorModule } from "./location-selector/location-selector.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    RouterModule,
    PopoverModule,
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    LoadingModule,
    AlertModule.forRoot(),
    LoginInfoModule,
    LogoutModule,
    UserCoreModule,
    LayoutModule,
    LocationSelectorModule,
  ],
  declarations: [
  ],
  exports: [
    CommonModule, FormsModule, HttpModule, RouterModule,

    // ModalModule,
    // ButtonsModule,
    // TooltipModule,
    BsDropdownModule,
    PopoverModule,
    AlertModule,
    // TabsModule,
    // AccordionModule,
    // CarouselModule,
    I18nModule,
    UtilsModule,
    SmartadminFormsModule,
    SmartProgressbarModule,
    LoadingModule,
    LayoutModule
  ],
  providers: [
    JsonApiService,
    LayoutService,
    AuthGuard,
  ]
})
export class SharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [JsonApiService, LayoutService]
    };
  }

}
