/**
 * This function coerces a string into a string literal type.
 * Using tagged union types in TypeScript 2.0, this enables
 * powerful typechecking of our reducers.
 * 
 * Since every action label passes through this function it
 * is a good place to ensure all of our action labels
 * are unique.
 */

let typeCache: { [label: string]: boolean } = {};
export function type<T>(label: T | ''): T {
    if (typeCache[<string>label]) {
        throw new Error(`Action type "${label}" is not unique"`);
    }

    typeCache[<string>label] = true;

    return <T>label;
}


// utility for comparing objects for identity, useful for situations where
// you want to see if two objects are really the same instance
// you can use it like  console.log( objectId(o1) )  => returns an instance-specific number
const objIdMap = new WeakMap;
let objectCount = 0;
export function objectId(object) {
    if (!objIdMap.has(object)) {
        objIdMap.set(object, ++objectCount);
    }
    return objIdMap.get(object);
}