

# Development Practices

## Overview

## Branching, Checkins, and Reviews

Mostly for the sake of sharing what we've learned while writing some new code, we'd like to try
using merge requests and the corresponding review that goes with it when checking in new code.

This means doing most development on a git branch.  We could go the route of master/development/feature/personal branches, 
but that's overkill for the size of the team, and considering that we're not trying to protect production-quality 
code on master.  Here's what we'll do:

Individual work should begin on a personal dev branch, with a name like "dev.paul".  

If you are working with someone else on a branch-worthy feature, then create a feature branch, then create a "feature.<yourname>" branch off of that for your work.  The feature branch can merge into master when it's merge-worthy. 

To create a branch, do `git checkout -b YourNameBranchName`

The first time you go to push your change to origin, you need to create it also on origin, like so:

`git push --set-upstream origin YourBranchName`

After pushing those changes up to the gitlab master, you can do a merge request there, 
which lets you pick a person to do a code review.

But... we can have only one reviewer.  If you want more "reviewers" send them a link to the change set.  You could also reference the person with an @username in the description of the merge request. (I think that will result in a notification to them, if they are signed up for notifications.)





