How to run


Running the Angular Client
--------------------------

  download and install node.js
  
  install angular cli: 
     npm install -g @angular/cli

  install all libraries required by app:
     cd WorkoutApp\client
     npm install

  install the json server (for serving up mock data via REST)
     npm install -g json-server

  start the json-server RESTful services
    cd WorkoutApp\mock-services
    json-server --watch db.json

  (in a new shell window) 
  compile and serve up the application (from workoutapp\client folder):
     ng serve 
     
  point your browser at the web app, "localhost:4200"

Running the Real Java Implementation of the Server
--------------------------------------------------

  download and install PostGres, start it up
  download and install maven (?)
  cd WorkoutApp\server
  build and unit test the services:
    mvn clean test 
  start the services up:
    mvn exec:java

    

