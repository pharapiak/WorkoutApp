<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1490990447301" ID="ID_1776403596" MODIFIED="1499287202996">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Workout App
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1473113631981" ID="ID_1856246112" MODIFIED="1490990659564" POSITION="left" STYLE="fork" TEXT="directory structure">
<edge COLOR="#808080" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1473113644893" ID="ID_724730624" MODIFIED="1490990659564" STYLE="fork" TEXT="WorkoutApp/">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1473113613229" ID="ID_1796144757" MODIFIED="1490990659564" STYLE="fork" TEXT="our repository is called WorkoutApp">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1473113658870" ID="ID_1955760005" MODIFIED="1490990659564" STYLE="fork" TEXT="holds the .git repository directory, and all of the working directories">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1473113684434" ID="ID_47032830" MODIFIED="1490990659565" STYLE="fork" TEXT="server">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1473113690192" ID="ID_1379382218" MODIFIED="1490970901016" STYLE="fork" TEXT="the Eclipse project dirs, in a maven hierarchy">
<edge COLOR="#808080"/>
</node>
</node>
<node COLOR="#990000" CREATED="1473113703101" ID="ID_1015239111" MODIFIED="1490990659565" STYLE="fork" TEXT="EclipseWorkspace">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1473113712143" ID="ID_750025021" MODIFIED="1490970901015" STYLE="fork" TEXT="the Eclipse workspace dir, create this yourself outside of this project tree">
<edge COLOR="#808080"/>
</node>
</node>
<node COLOR="#990000" CREATED="1489067196702" ID="ID_359281487" MODIFIED="1490990659565" TEXT="client">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1489067230437" ID="ID_284861102" MODIFIED="1490970901015" TEXT="holds the Angular application"/>
</node>
<node COLOR="#990000" CREATED="1473113639358" ID="ID_1764190589" MODIFIED="1490990659565" STYLE="fork" TEXT=".git">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1489067271828" ID="ID_1171795951" MODIFIED="1490970901014" TEXT="the folder that holds the local git repository"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1474779731921" FOLDED="true" ID="ID_1835590870" MODIFIED="1495070433317" POSITION="left" STYLE="fork" TEXT="app server">
<edge COLOR="#808080" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1474779785430" ID="ID_744253442" LINK="https://grizzly.java.net/documentation.html" MODIFIED="1490990659565" TEXT="Grizzly">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1474779790319" ID="ID_1939249763" MODIFIED="1490990659565" TEXT="high performance app server">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1474779862351" ID="ID_488843066" MODIFIED="1490990659565" TEXT="seems to be built into Eclipse tools?">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1477626656583" ID="ID_810823008" MODIFIED="1490990659565" TEXT="app server for GlassFish">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1474779739126" ID="ID_1658147566" MODIFIED="1490990659565" TEXT="Tomcat">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1474779757890" ID="ID_583964373" MODIFIED="1490990659566" TEXT="sounds like it can handle all the needs of a RESTfull api">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1477626090763" ID="ID_537411918" MODIFIED="1490990659566" TEXT="installation notes:">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1477626109738" ID="ID_977340112" LINK="http://tomcat.apache.org/download-90.cgi?Preferred=http%3A%2F%2Fapache.mirror.iweb.ca%2F" MODIFIED="1477829835851" TEXT="downloaded windows 64 bit zip from here"/>
<node COLOR="#111111" CREATED="1477626155589" ID="ID_1400242076" MODIFIED="1477829835851" TEXT="unzipped, and moved tomcat folder to c:"/>
<node COLOR="#111111" CREATED="1477626179491" ID="ID_109966427" MODIFIED="1477829835851" TEXT="read the RUNNING.txt file">
<node COLOR="#111111" CREATED="1477626208974" ID="ID_1663814571" MODIFIED="1477829835866" TEXT="set env vars:">
<node COLOR="#111111" CREATED="1477626220784" ID="ID_535517403" MODIFIED="1477829835866" TEXT="CATALINA_HOME=c:\tomcat...."/>
<node COLOR="#111111" CREATED="1477626234974" ID="ID_27935726" MODIFIED="1477829835866" TEXT="check for JAVA_HOME or JRE_HOME"/>
</node>
<node COLOR="#111111" CREATED="1477626413830" ID="ID_1250889412" MODIFIED="1477829835866" TEXT="start up with C:\apache-tomcat-9.0.0.M11\bin&gt;startup.bat"/>
</node>
<node COLOR="#111111" CREATED="1477626512751" ID="ID_1652002104" MODIFIED="1477829835851" TEXT="connect at http://localhost:8080/"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1474779800746" ID="ID_1438290648" MODIFIED="1490990659566" TEXT="GlassFish">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1474779811434" ID="ID_1463318060" MODIFIED="1490990659566" TEXT="J2EE (enterpriser srvices) app server, including support for legacy stuff like SOAP and CORBA">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1474779915312" ID="ID_909099739" MODIFIED="1490990659566" TEXT="GUI admin console">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1474779954302" ID="ID_629934393" MODIFIED="1490990659566" TEXT="owned by Oracle (but open source?)">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1474779837682" ID="ID_979441624" MODIFIED="1490990659566" TEXT="JBoss">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1474779842968" ID="ID_1926748873" MODIFIED="1490990659566" TEXT="similar to GlassFish">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1474779927166" ID="ID_1639137473" MODIFIED="1490990659566" TEXT="command line admin">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1474779938694" ID="ID_1824676177" MODIFIED="1490990659566" TEXT="Open source community driven">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1466250101197" ID="ID_984410536" MODIFIED="1492090933532" POSITION="right" STYLE="fork" TEXT="tools">
<edge COLOR="#808080" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1466250110653" FOLDED="true" ID="ID_812885510" MODIFIED="1500351691727" STYLE="fork" TEXT="git source control">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1466248845234" ID="ID_621444583" MODIFIED="1490990659567" STYLE="fork" TEXT="Git setup">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1466248862334" ID="ID_1575377023" MODIFIED="1487651590012" STYLE="fork" TEXT="using Gitlab">
<edge COLOR="#808080"/>
</node>
<node COLOR="#111111" CREATED="1466248870104" ID="ID_874837401" MODIFIED="1487651590011" STYLE="fork" TEXT="need to create SSH key">
<edge COLOR="#808080"/>
<node COLOR="#111111" CREATED="1466248892254" ID="ID_1693832957" LINK="http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html" MODIFIED="1489027296239" STYLE="fork" TEXT="install putty, it has a tool to do so">
<edge COLOR="#808080"/>
</node>
<node COLOR="#111111" CREATED="1489027333168" ID="ID_1273598097" MODIFIED="1490970901040" TEXT="generate key">
<node COLOR="#111111" CREATED="1489027352002" ID="ID_167858459" MODIFIED="1490970901040" TEXT="open the git bash window"/>
<node COLOR="#111111" CREATED="1489027363613" ID="ID_150718512" LINK="https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/" MODIFIED="1490970901040" TEXT="execute ssh-keygen"/>
<node COLOR="#111111" CREATED="1489027866742" ID="ID_1638621244" MODIFIED="1490970901040" TEXT="add the ssh key to your git account"/>
<node COLOR="#111111" CREATED="1489027911205" ID="ID_465088124" MODIFIED="1490970901039" TEXT="copy the ssh key to your clipboard:">
<node COLOR="#111111" CREATED="1489027927449" ID="ID_1448320799" MODIFIED="1490970901039" TEXT="clip &lt; ~/.ssh/id_rsa.pub"/>
</node>
<node COLOR="#111111" CREATED="1489028084030" ID="ID_1490922325" LINK="https://gitlab.com/profile/keys" MODIFIED="1490970901039" TEXT="and paste it to your gitlab acount (after you&apos;ve logged in there)"/>
</node>
</node>
<node COLOR="#111111" CREATED="1489026055692" ID="ID_651712801" LINK="https://github.com/git-for-windows/git/releases/download/v2.12.0.windows.1/Git-2.12.0-64-bit.exe" MODIFIED="1490970901038" TEXT="download"/>
<node COLOR="#111111" CREATED="1489028552149" ID="ID_454175428" MODIFIED="1490970901035" TEXT="setup BeyondCompare as the diff and merge tool">
<node COLOR="#111111" CREATED="1489028588068" ID="ID_1040640784" MODIFIED="1489028588068">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <font color="rgb(127, 101, 39)" face="monospace" size="12.04px"><span style="text-align: start; font-family: monospace; text-indent: 0px; display: inline !important; font-weight: normal; color: rgb(127, 101, 39); word-spacing: 0px; float: none; background-color: rgb(247, 247, 247); white-space: normal; letter-spacing: normal; text-transform: none; font-style: normal; font-size: 12.04px">git config --global diff.tool bc3</span></font>
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1489028609742" ID="ID_862659412" MODIFIED="1490970901035" TEXT=" git config --global difftool.bc3.path &quot;c:/Program Files/Beyond Compare 4/bcomp.exe&quot;"/>
<node COLOR="#111111" CREATED="1489028629559" ID="ID_649151976" MODIFIED="1490970901035" TEXT="git config --global merge.tool bc3"/>
<node COLOR="#111111" CREATED="1489028644805" ID="ID_1075572745" MODIFIED="1490970901034" TEXT=" git config --global mergetool.bc3.path &quot;c:/Program Files/Beyond Compare 4/bcomp.exe&quot;"/>
<node COLOR="#111111" CREATED="1489028730634" ID="ID_1650528154" MODIFIED="1490970901031" TEXT="(then, to merge, use &quot;git difftool&quot;, or  &quot;git mergetool&quot; , plus file names, or nothing to merge all outstanding files in turn"/>
</node>
</node>
<node COLOR="#990000" CREATED="1466250125502" ID="ID_1581493468" LINK="https://git-scm.com/book/en/v1/Getting-Started" MODIFIED="1490990659567" STYLE="fork" TEXT="getting started">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1471991861541" ID="ID_710379260" MODIFIED="1490990659567" STYLE="fork" TEXT="integration with Eclipse">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1471991874387" ID="ID_1768611585" LINK="http:/wiki.eclipse.org/EGit/User_Guide" MODIFIED="1487651590010" STYLE="fork" TEXT="see EGit User Guide">
<edge COLOR="#808080"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1487683406008" FOLDED="true" ID="ID_1578579355" MODIFIED="1500351692667" TEXT="angular-cli">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1487683415908" ID="ID_1521894880" MODIFIED="1490990659567" TEXT="tool for generating components, modules, projects">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1487683435093" ID="ID_70145179" LINK="https://github.com/angular/angular-cli" MODIFIED="1490990659567" TEXT="install and basic info">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1465169728149" FOLDED="true" ID="ID_32396873" MODIFIED="1500351693324" STYLE="fork" TEXT="Install Node.js">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1465169810024" ID="ID_1220059583" LINK="https:/nodejs.org/en/download" MODIFIED="1490990659568" STYLE="fork" TEXT="download">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1465169748823" FOLDED="true" ID="ID_198616827" MODIFIED="1500351695334" STYLE="fork" TEXT="Angular 2.0">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1465169755203" ID="ID_875579134" MODIFIED="1490990659568" STYLE="fork" TEXT="getting started">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1465169759646" ID="ID_28107858" MODIFIED="1489067330167" TEXT="https://angular.io/docs/ts/latest/quickstart.html">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1481820128218" ID="ID_580214893" MODIFIED="1490990659568" STYLE="fork" TEXT="state management">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1481820136169" FOLDED="true" ID="ID_448022728" MODIFIED="1489067330167" STYLE="fork" TEXT="redux pattern">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1481820177910" ID="ID_1518988776" MODIFIED="1481820187518" TEXT="store">
<node COLOR="#111111" CREATED="1481820188127" ID="ID_935640576" MODIFIED="1481820197618" TEXT="holds state directly"/>
<node COLOR="#111111" CREATED="1481820309802" ID="ID_1863486643" MODIFIED="1481820355149" TEXT="gets initialized via app bootstrap"/>
<node COLOR="#111111" CREATED="1481821004924" ID="ID_387055256" MODIFIED="1481821012423" TEXT="holds immutable state">
<node COLOR="#111111" CREATED="1481821013389" ID="ID_1340797792" MODIFIED="1481821022399" TEXT="but new versions are created by reducers"/>
</node>
<node COLOR="#111111" CREATED="1481918918838" ID="ID_72502978" MODIFIED="1481918947903" TEXT="(ngrx) offers up Observables on that state via store.select(topic)"/>
</node>
<node COLOR="#111111" CREATED="1481820142501" ID="ID_1071305410" MODIFIED="1481820167044" TEXT="reducer">
<node COLOR="#111111" CREATED="1481820167932" ID="ID_1041487293" MODIFIED="1481820941190" TEXT="handles request to change state"/>
<node COLOR="#111111" CREATED="1481820915497" ID="ID_110345822" MODIFIED="1481820929287" TEXT="functions, not class">
<node COLOR="#111111" CREATED="1481820970352" ID="ID_1080430804" MODIFIED="1481820979947" TEXT="returns modified state item"/>
</node>
<node COLOR="#111111" CREATED="1481821230822" ID="ID_817893855" MODIFIED="1481821252447" TEXT="operations on set">
<node COLOR="#111111" CREATED="1481821253028" ID="ID_261491721" MODIFIED="1481821355198" TEXT="create =&gt; return new collection with new item added"/>
<node COLOR="#111111" CREATED="1481821265143" ID="ID_201024064" MODIFIED="1481821274551" TEXT="set items =&gt; return new collection"/>
<node COLOR="#111111" CREATED="1481821275303" ID="ID_1602634203" MODIFIED="1481821287892" TEXT="delete item =&gt; return new collection with item missing"/>
<node COLOR="#111111" CREATED="1481821288439" ID="ID_779792670" MODIFIED="1481821338402" TEXT="update item =&gt; return new collection with old item replaced with new"/>
</node>
<node COLOR="#111111" CREATED="1481821400515" ID="ID_1385150" MODIFIED="1481821424214" TEXT="invoked with &quot;this.store.dispatch(reducer)&quot;"/>
</node>
<node COLOR="#111111" CREATED="1481820199724" ID="ID_911251755" MODIFIED="1481820209505" TEXT="Service">
<node COLOR="#111111" CREATED="1481820210474" ID="ID_725829877" MODIFIED="1481820231407" TEXT="pushes state to store?"/>
<node COLOR="#111111" CREATED="1481820232646" ID="ID_1876307061" MODIFIED="1481918962460" TEXT="holds reference to state in store via observable (or not?)"/>
<node COLOR="#111111" CREATED="1481820243925" ID="ID_929255078" MODIFIED="1481820260906" TEXT="exposes that observable to components"/>
<node COLOR="#111111" CREATED="1481820370374" ID="ID_1191356336" MODIFIED="1481820379588" TEXT="has store injected into ctor"/>
<node COLOR="#111111" CREATED="1481821450173" ID="ID_1086546458" MODIFIED="1481821471098" TEXT="has methods to request store to be loaded, vs getters?"/>
</node>
<node COLOR="#111111" CREATED="1481820262406" ID="ID_1618680187" MODIFIED="1481820265175" TEXT="components">
<node COLOR="#111111" CREATED="1481820265680" ID="ID_811833151" MODIFIED="1483841893612" TEXT="observing state from ...">
<node COLOR="#111111" CREATED="1481918973859" ID="ID_697078042" MODIFIED="1483841882522" TEXT="a slice of the store"/>
<node COLOR="#111111" CREATED="1481919010231" ID="ID_1048206749" MODIFIED="1483841937244" TEXT="routable components have observable, and push latest state to child"/>
</node>
<node COLOR="#111111" CREATED="1481820273381" ID="ID_1980923331" MODIFIED="1481919032122" TEXT="holds references to state via observables... or not"/>
</node>
<node COLOR="#111111" CREATED="1481820430606" ID="ID_1604482834" MODIFIED="1481910501599" TEXT="misc">
<node COLOR="#111111" CREATED="1481820437483" ID="ID_1779631702" MODIFIED="1481820471942" TEXT="store.select(&apos;items&apos;) returns observable on items? how?  Where does select come from?"/>
<node COLOR="#111111" CREATED="1481910505509" ID="ID_1439077840" MODIFIED="1481910527211" TEXT="deepfreeze is testing tool to ensure state is not mutated"/>
</node>
<node COLOR="#111111" CREATED="1482376538335" ID="ID_572649679" MODIFIED="1482376552018" TEXT="using ngrx/store, and related tools, like:">
<node COLOR="#111111" CREATED="1482376555808" ID="ID_319745828" MODIFIED="1482376560828" TEXT="time travelling debugger"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1490191065338" ID="ID_130161023" MODIFIED="1490990659568" TEXT="good guide books">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1490191081930" ID="ID_995908807" LINK="https://angular-2-training-book.rangle.io/handout/modules/shared-modules-di.html" MODIFIED="1490970901029" TEXT="rangle.io"/>
<node COLOR="#111111" CREATED="1490191091409" ID="ID_524133299" LINK="https://angular.io/docs/ts/latest/" MODIFIED="1490970901029" TEXT="angular.io - main site"/>
</node>
<node COLOR="#990000" CREATED="1490970630281" ID="ID_1360273286" MODIFIED="1490990659568" TEXT="use of barrels (index.ts) within modules">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1490970675341" ID="ID_878600198" LINK="https://github.com/angular/angular.io/issues/1301" MODIFIED="1490970901026" TEXT="discussion here"/>
<node COLOR="#111111" CREATED="1490970646122" ID="ID_125518901" MODIFIED="1490970901022" TEXT="I have so far found the following tactic working well, when applied consistently: &gt;1 intra-module (inside) imports need to reference individual files in import &gt;2 a module definition references only the barrel file (so that the barrel API surface==ngModule surface) &gt;3 a barrel file never exports an ngModule &gt;4 inter-module (outside) dependencies only references other module&apos;s code via the module&apos;s barrel (ensure that ngModule dependecies==barrel dependencies)  This way allows me to think of the barell file like a *.d.ts file of the whole module. It has resolved the circular dependency issues I was having. I really like having a barrel file for shared modules as it makes my other code considerably cleaner."/>
</node>
<node COLOR="#990000" CREATED="1490970705081" ID="ID_111478856" MODIFIED="1490990659568" TEXT="redux, reactive, and ngrx/store">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1490970726196" ID="ID_1073911559" LINK="https://github.com/ngrx/store/issues/197" MODIFIED="1490970901022" TEXT="ngrx/store and lazy load modules"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1489067381089" ID="ID_1305333115" MODIFIED="1490990659568" TEXT="Visual Studio Code">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1489067400444" ID="ID_1207746932" MODIFIED="1490990659568" TEXT="extensions">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1490990200749" ID="ID_363885065" MODIFIED="1490990659569" TEXT="ng2-bootstrap">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1490990268452" ID="ID_154774538" MODIFIED="1490990659569" TEXT="lots of tools for responsive dropdown lists, menus, etc">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1490990246276" ID="ID_974598394" LINK="http://valor-software.com/ng2-bootstrap/#/dropdowns#usage" MODIFIED="1490990659569" TEXT="reference guide ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1466248922775" ID="ID_7370520" MODIFIED="1496325984177" TEXT="Eclipse">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1466248926394" ID="ID_1360008822" MODIFIED="1496325984178" STYLE="fork" TEXT="using version ... ?">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1499287221330" ID="ID_835484225" MODIFIED="1499287223534" TEXT="postgres">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1499287224584" ID="ID_1523343662" LINK="https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows" MODIFIED="1499287240000" TEXT="download from enterprisedb.com">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1499287950353" ID="ID_1627431889" MODIFIED="1499287976781" TEXT="I installed version 9.6.3 (windows 64 bit)"/>
</node>
<node COLOR="#990000" CREATED="1499287243584" ID="ID_1136330872" MODIFIED="1499287256038" TEXT="includes pgadmin for managing DBs">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1500351708661" ID="ID_1382616395" MODIFIED="1500351712344" TEXT="ngrx">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1500351713235" ID="ID_1178106393" MODIFIED="1500351734617" TEXT="centralized app state, watched with Observables">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1500351735891" ID="ID_1615177534" MODIFIED="1500351745295" TEXT="see ReadMe.md for more info">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1500351746204" ID="ID_1314620824" MODIFIED="1500351750335" TEXT="Notes:">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1500351751147" ID="ID_335335952" MODIFIED="1500351765368" TEXT="create selector that takes an argument:">
<node COLOR="#111111" CREATED="1500351766971" ID="ID_1211318492" MODIFIED="1500351771456" TEXT="    // this create a selector that takes an id argument, see here:     // https://github.com/reactjs/reselect#q-how-do-i-create-a-selector-that-takes-an-argument     static getEquipmentById = createSelector(         EquipmentSelectors.getAllEquipment,         allEquipment =&gt; memoize(             id =&gt; allEquipment.filter(equipment =&gt; equipment.id === id)         )     ); "/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1490990544123" ID="ID_924542487" MODIFIED="1495070414247" POSITION="left" TEXT="other Exercise Software">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1480469007608" FOLDED="true" ID="ID_1151586988" MODIFIED="1490990366029" STYLE="fork" TEXT="Workout Manager: Open Source">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node BACKGROUND_COLOR="#ffff00" COLOR="#990000" CREATED="1480469027671" ID="ID_521459482" MODIFIED="1490990366030" TEXT=" https://wger.de">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffff00" COLOR="#990000" CREATED="1480469142304" ID="ID_1009143366" LINK="http://wger.de/api/v2/" MODIFIED="1490990366030" TEXT="REST API">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node BACKGROUND_COLOR="#ffff00" COLOR="#111111" CREATED="1480469149747" ID="ID_938967669" MODIFIED="1490990366030" TEXT="http://wger.de/api/v2/exercise/">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ffff00" COLOR="#111111" CREATED="1480469152721" ID="ID_133698491" MODIFIED="1490990366031" TEXT="got a consumer key:">
<font NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ffff00" COLOR="#111111" CREATED="1480469175488" ID="ID_609939824" MODIFIED="1487620331959" TEXT="2183aa60eb0389b057e800a2cef5a318bea327c9"/>
</node>
<node BACKGROUND_COLOR="#ffff00" COLOR="#111111" CREATED="1480469290105" ID="ID_1576096979" MODIFIED="1490990366031">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="../../../tmp/Capture.JPG" />
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ffff00" COLOR="#111111" CREATED="1480469572030" ID="ID_1731803589" MODIFIED="1490990366031" TEXT="Example URL">
<font NAME="SansSerif" SIZE="12"/>
<node BACKGROUND_COLOR="#ffff00" COLOR="#111111" CREATED="1480469580892" ID="ID_53775876" MODIFIED="1487620331960" TEXT=" http://wger.de/api/v2/exercise?language=2&amp;limit=20"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1495070414248" ID="ID_1585375783" LINK="http://www.bodbot.com/Training.html" MODIFIED="1495070423131" TEXT="BodBot">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1474775079678" ID="ID_1306273537" MODIFIED="1490990659569" POSITION="left" STYLE="fork" TEXT="RESTful services">
<edge COLOR="#808080" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1474775170947" ID="ID_1082249120" MODIFIED="1490990659569" STYLE="fork" TEXT="using Jersey tools">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1474775187495" ID="ID_1950460557" LINK="https://jersey.java.net/documentation/latest/getting-started.html" MODIFIED="1490990659570" TEXT="getting started">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1474775213633" ID="ID_687100018" MODIFIED="1490990659570" TEXT="used mvn &quot;archetype&quot; to generate eclipse project">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1474776961409" ID="ID_656138036" MODIFIED="1474781376910" TEXT="with this tweaked command"/>
<node COLOR="#111111" CREATED="1474776990157" ID="ID_1216172514" MODIFIED="1474781376909">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      mvn archetype:generate -DarchetypeArtifactId=jersey-quickstart-grizzly2 \
    </p>
    <p>
      -DarchetypeGroupId=org.glassfish.jersey.archetypes -DinteractiveMode=false \
    </p>
    <p>
      -DgroupId=com.sumac.workout.service -DartifactId=service -Dpackage=com.sumac.workout.service \
    </p>
    <p>
      -DarchetypeVersion=2.23.2
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#111111" CREATED="1477631122505" ID="ID_1682903969" LINK="http://www.oracle.com/webfolder/technetwork/tutorials/obe/java/basic_grizzly_jersey/jersey-grizzly-json-service.html" MODIFIED="1477829845678" TEXT="better archetype here:">
<node COLOR="#111111" CREATED="1477830218072" ID="ID_690090681" MODIFIED="1487620331857" TEXT="yeah, and this one works with json!"/>
<node COLOR="#111111" CREATED="1477830738983" ID="ID_196981529" MODIFIED="1487620331858" TEXT="diff with my workout example:">
<node COLOR="#111111" CREATED="1477830751724" ID="ID_1974671307" MODIFIED="1487620331859" TEXT="&lt;jersey.version&gt;2.23.2&lt;/jersey.version&gt; -fail"/>
<node COLOR="#111111" CREATED="1477830751724" ID="ID_309009952" MODIFIED="1487620331859" TEXT="&lt;jersey.version&gt;2.17&lt;/jersey.version&gt; - success"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1474776938231" ID="ID_1612678614" MODIFIED="1490990659570" TEXT="then built and tested with &quot;mvn clean test&quot;">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1474777051282" ID="ID_1338971059" MODIFIED="1474781376909" TEXT="success!"/>
</node>
<node COLOR="#990000" CREATED="1474777099068" ID="ID_1804250113" MODIFIED="1490990659570" TEXT="start it up with &quot;mvn exec:java&quot;">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1477626687569" ID="ID_137767218" LINK="http://www.oracle.com/webfolder/technetwork/tutorials/obe/java/griz_jersey_intro/Grizzly-Jersey-Intro.html" MODIFIED="1490990659570" TEXT="another intro here">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1477830191079" ID="ID_3802128" MODIFIED="1490990659570" TEXT="debugging">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1480542009871" ID="ID_655048961" MODIFIED="1490990659571" TEXT="had to add support for CORS, using ">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1480542033900" ID="ID_1604286528" MODIFIED="1487651589989" TEXT="http://software.dzhuvinov.com/cors-filter.html"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1474781338146" ID="ID_1248463672" MODIFIED="1490990659571" STYLE="fork" TEXT="REST basics">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1474781344960" ID="ID_1782871251" LINK="http://www.restapitutorial.com/lessons/restfulresourcenaming.html" MODIFIED="1490990659571" STYLE="fork" TEXT="naming">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1474781549287" ID="ID_1857900305" MODIFIED="1490990659572" STYLE="fork" TEXT="standardized Java API:  JAX-RS (Restful Services)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1487619921252" ID="ID_489138912" LINK="https://www.npmjs.com/package/json-server" MODIFIED="1490990659572" STYLE="fork" TEXT="mock services with json-server">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1487620078910" ID="ID_1789219392" MODIFIED="1490990659572" STYLE="fork" TEXT="install">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1487620094405" ID="ID_1392252334" MODIFIED="1487651589981" STYLE="fork" TEXT="npm install -g json-server">
<edge COLOR="#808080"/>
</node>
</node>
<node COLOR="#990000" CREATED="1487620064873" ID="ID_1679282466" MODIFIED="1490990659572" STYLE="fork" TEXT="running">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1487620249526" ID="ID_1746258098" MODIFIED="1487651589973" STYLE="fork" TEXT="seed data in db.json">
<edge COLOR="#808080"/>
<node COLOR="#111111" CREATED="1487620267717" ID="ID_246715974" MODIFIED="1487651589970" STYLE="fork" TEXT="eg:">
<edge COLOR="#808080"/>
<node COLOR="#111111" CREATED="1487620276937" ID="ID_933514336" MODIFIED="1487651589962" STYLE="fork" TEXT="{   &quot;posts&quot;: [     { &quot;id&quot;: 1, &quot;title&quot;: &quot;json-server&quot;, &quot;author&quot;: &quot;typicode&quot; }   ],   &quot;comments&quot;: [     { &quot;id&quot;: 1, &quot;body&quot;: &quot;some comment&quot;, &quot;postId&quot;: 1 }   ],   &quot;profile&quot;: { &quot;name&quot;: &quot;typicode&quot; } }">
<edge COLOR="#808080"/>
</node>
</node>
<node COLOR="#111111" CREATED="1487620587159" ID="ID_291032421" MODIFIED="1487651589962" TEXT="&quot;golden&quot; copy is in src/mock-data/db.json"/>
<node COLOR="#111111" CREATED="1487620613357" ID="ID_989912359" MODIFIED="1487651589961" TEXT="copy this file to src for testing (.gitignore will skip this copy)"/>
<node COLOR="#111111" CREATED="1487620646297" ID="ID_1739588773" MODIFIED="1487651589961" TEXT="if it&apos;s good, update the src/mock-data copy">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1487619945799" ID="ID_858391043" MODIFIED="1487651589960" STYLE="fork" TEXT="json-server --watch db.json">
<edge COLOR="#808080"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1493827400792" ID="ID_1711800559" MODIFIED="1493827404819" POSITION="right" TEXT="security">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1493827407858" ID="ID_751122423" MODIFIED="1493827415914" TEXT="&quot;stay logged in&quot;">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1493827418034" ID="ID_1953777654" LINK="http://stackoverflow.com/questions/1354999/keep-me-logged-in-the-best-approach" MODIFIED="1493827454600" TEXT="possible implementation">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1496234308443" ID="ID_1267638414" MODIFIED="1496325870819" TEXT="OAuth">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1496234278829" ID="ID_1799572668" MODIFIED="1496234339264" TEXT="does this avoid login every time the app is reloaded?">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1496234341003" ID="ID_1709912551" MODIFIED="1496234364239" TEXT="also allows us to use facebook, etc. for authenticating users">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1496325220354" ID="ID_980645857" MODIFIED="1496325222546" TEXT="libraries">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1496325193769" ID="ID_307523458" MODIFIED="1496325225034" TEXT="client">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1496325197706" ID="ID_87586818" LINK="https://www.npmjs.com/package/angular-oauth2-oidc" MODIFIED="1496325210801" TEXT="https://www.npmjs.com/package/angular-oauth2-oidc"/>
</node>
<node COLOR="#111111" CREATED="1496325227818" ID="ID_1410500536" MODIFIED="1496325229503" TEXT="server">
<node COLOR="#111111" CREATED="1496325230621" ID="ID_407918619" MODIFIED="1496325233489" TEXT="do we need this?"/>
</node>
</node>
<node COLOR="#990000" CREATED="1496325870820" ID="ID_1370579834" MODIFIED="1496325883746" TEXT="guidelines">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1496325885994" ID="ID_115763560" LINK="https://oauth.net/getting-started/" MODIFIED="1496325904400" TEXT="getting started from oauth.net"/>
<node COLOR="#111111" CREATED="1496325930229" ID="ID_303902088" LINK="https://developers.google.com/identity/protocols/OAuth2UserAgent" MODIFIED="1496325942111" TEXT="google&apos;s guidelines"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1474780237185" ID="ID_81020429" MODIFIED="1490990659573" POSITION="left" STYLE="fork" TEXT="Cloud hosting">
<edge COLOR="#808080" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1474780244536" ID="ID_687091527" LINK="https://www.heroku.com/home" MODIFIED="1490990659573" STYLE="fork" TEXT="Heroku">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
</node>
</map>
