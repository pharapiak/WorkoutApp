package com.sumac.workout.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

//@Entity
//@Inheritance(strategy=InheritanceType.JOINED)
@MappedSuperclass
public abstract class DomainModel {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	protected Long id;

    @Version
    private Integer version;

 	public Long getId() {
		return id;
	}

	public void setId(Long id) {
	    this.id = id;
	}
	
    public Integer getVersion() { return version; }	

}
