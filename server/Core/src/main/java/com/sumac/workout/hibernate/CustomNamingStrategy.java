package com.sumac.workout.hibernate;

import org.hibernate.cfg.ImprovedNamingStrategy;


/**
 * A custom naming strategy implementation which uses following naming conventions:
 * <ul>
 *     <li>Table names are lower case and in singular form.</li>
 *     <li>Column names are lower case and words are separated with '_' character.  Any leading underscore on the 
 *     field name will be removed.</li>
 * </ul>
 */
public class CustomNamingStrategy extends ImprovedNamingStrategy {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5479653821799809368L;

	@Override
	public String columnName(String columnName) {
		if (columnName.startsWith("_")) columnName.substring(1,columnName.length());
		return super.columnName(columnName);
	}

 

}