package com.sumac.workout.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.sumac.workout.domain.DomainModel;
import com.sumac.workout.services.DataStore;
import com.sumac.workout.services.PersistServices;
import com.sumac.workout.hibernate.ScrollableResultsList;

import javassist.tools.rmi.ObjectNotFoundException;



public abstract class HibernateDataStore<DomainModelType extends DomainModel> implements DataStore<DomainModelType> {

	private PersistServices _persistServices;
	
	protected PersistServices getPersistServices() {
		if (_persistServices==null) _persistServices=new HibernatePersistServices();
		return _persistServices;
	}

	protected void setPersistServices(PersistServices _persistServices) {
		this._persistServices = _persistServices;
	}
	
		/* (non-Javadoc)
	 * @see com.sumac.workout.hibernate.IDataStore#store(DomainModelType)
	 */
	public long store(DomainModelType obj) throws Exception {
		Session session = getPersistServices().getSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.save(obj);
			transaction.commit();
		} catch (Exception ex){
			transaction.rollback();
			throw ex;
		}
		return obj.getId();
	}

	/* (non-Javadoc)
	 * @see com.sumac.workout.hibernate.IDataStore#findAll()
	 */
	public Iterator<DomainModelType> findAll() {
		Session session = getPersistServices().getSession();
		Transaction transaction = session.beginTransaction();
		try {
			Query query = session.createQuery("From " + getTableName());
	        query.setFetchSize(Integer.valueOf(1000));
	        query.setReadOnly(true);
	        query.setLockMode("a", LockMode.NONE);
	        ScrollableResults results = query.scroll(ScrollMode.FORWARD_ONLY);
			Iterator<DomainModelType> it = new ScrollableResultsList<DomainModelType>(session, results).iterator();
			ArrayList<DomainModelType> result =  new ArrayList<DomainModelType>();
	        //copy out the results so we can pass it out of this transaction
	        it.forEachRemaining(result::add);
	        return result.iterator();
		} finally {
			transaction.rollback();
		}
	}

	/* (non-Javadoc)
	 * @see com.sumac.workout.hibernate.IDataStore#findById(long)
	 */
	public DomainModelType findById(long id) throws ObjectNotFoundException {
		Session session = getPersistServices().getSession();
		Transaction transaction = session.beginTransaction();
		try {
			Query q = getPersistServices().getSession().createQuery("From " + getTableName() +" where id = " +id);
	
			@SuppressWarnings("unchecked")
			List<DomainModelType> resultList = q.list();
			if (resultList.size()==0) {
				throw new ObjectNotFoundException("DomainObject id "+id+" not found.");
			}
			return resultList.get(0);
		} finally {
			transaction.rollback();
		}
	}

	protected abstract String getTableName();


}
