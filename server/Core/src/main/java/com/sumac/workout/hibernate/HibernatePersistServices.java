package com.sumac.workout.hibernate;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.sumac.workout.services.PersistServices;

public class HibernatePersistServices implements PersistServices {

	protected static ThreadLocal<Session> _session = new ThreadLocal<Session>() {
	   @Override protected Session initialValue() {
	       return HibernateUtil.getSessionFactory().openSession();
	   }
	};

	public HibernatePersistServices() {
	}

	public Session getSession(){
		return _session.get(); 
	}

	public org.hibernate.Transaction beginTransaction(){
		return getSession().beginTransaction();
	}
	
	public void commitTransaction(Transaction transaction) throws SecurityException, RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException{
		transaction.commit();
	}

	public void rollbackTransaction(Transaction transaction) throws IllegalStateException, SystemException{
		transaction.rollback();
	}
}
