package com.sumac.workout.hibernate;
  

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistryBuilder;

  
public class HibernateUtil {
  
    private static final SessionFactory sessionFactory = buildSessionFactory();
  
    private static SessionFactory buildSessionFactory() {
        try {
        	
             // Create the SessionFactory from hibernate.cfg.xml
            Configuration configuration = new Configuration();
            // set our custom naming strategy for stripping leading underscores from field names
            configuration.setNamingStrategy(new CustomNamingStrategy()); 
            configuration.configure();
            //TODO maybe have a registry of data stores, and have them addAnnotated classes they know about?
            // configuration.addAnnotatedClass(Exercise.class);
            ServiceRegistryBuilder builder=new ServiceRegistryBuilder().applySettings(configuration.getProperties());
            SessionFactory sessionFactory = configuration.buildSessionFactory(builder.buildServiceRegistry());
            return sessionFactory;
 
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
  
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
  
    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }
  
}