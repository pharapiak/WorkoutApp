package com.sumac.workout.services;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CreateException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 38498312672887264L;

	/**
	 * Create a HTTP 404 (Not Found) exception.
	 */
	public CreateException() {
		super(Response.status(Response.Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN).build());
	}

	/**
	 * Create a HTTP 404 (Not Found) exception.
	 * 
	 * @param message
	 *            the String that is the entity of the 404 response.
	 */
	public CreateException(String message) {
		super(Response.status(Response.Status.BAD_REQUEST).entity(message).type(MediaType.TEXT_PLAIN).build());
	}

	public CreateException(JsonError jse) {
		super(Response.status(Response.Status.BAD_REQUEST).entity(jse).type(MediaType.APPLICATION_JSON).build());
	}

}
