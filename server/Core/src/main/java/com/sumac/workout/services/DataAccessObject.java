package com.sumac.workout.services;

import java.util.Iterator;

import com.sumac.workout.domain.DomainModel;

import javassist.tools.rmi.ObjectNotFoundException;


public class DataAccessObject<DomainModelType extends DomainModel> {

	protected DataStore<DomainModelType> _dataStore;
	
	protected DataAccessObject(DataStore<DomainModelType> dataStore){
		_dataStore=dataStore;
	}
	
	
	protected DataStore<DomainModelType> getDataStore() {
		return _dataStore;
	}

	/**
	 * Save the DomainModel object to the database
	 * @param obj
	 * @return
	 * @throws Exception 
	 */
	public long save(DomainModelType obj) throws Exception {
		return getDataStore().store(obj);
	}
	
	public DomainModelType findById(long id) throws ObjectNotFoundException {
		return getDataStore().findById(id);
	}
	
	public Iterator<DomainModelType> findAll() {
		return getDataStore().findAll();
	}
	

}
