package com.sumac.workout.services;

import java.util.Iterator;
import com.sumac.workout.domain.DomainModel;

import javassist.tools.rmi.ObjectNotFoundException;

/**
 * Interface for storing and fetching domain models
 */
public interface DataStore<DomainModelType extends DomainModel> {
	
	/**
	 * Store the supplied object to the database, and return the generated id. 
	 *
	 * @param  obj  a transient domain object
	 * @return      unique id
	 * @throws Exception 
	 */
	public long store(DomainModelType obj) throws Exception;
	
	/**
	 * Fetch the object in the database that corresponds the the id. 
	 *
	 * @param  id   a domain object id
	 * @return      DomainModel
	 */
	public DomainModelType findById(long id) throws ObjectNotFoundException ;
	
	
	/**
	 * Fetch all instances of DomainModelType (and its subclasses)
	 * @return
	 */
	Iterator<DomainModelType> findAll();
	
}

