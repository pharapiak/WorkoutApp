package com.sumac.workout.domain.test;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

import com.sumac.workout.domain.DomainModel;
  
/**
 * Sample class for testing Hibernate mapping, base domain model classes, and base DAOs
 * @author PaulH
 *
 */
@Entity
public class Exrcise extends DomainModel {
	
    @Column(unique=true, nullable=false, length=200)
	private String _name;

    @Column(length=1200)
	private String description;
	
	@Basic(fetch = FetchType.LAZY)
	@Lob
	private byte[] demoVideo;
	
	@Column(length=800)
	private String tips;
	
	@Column(length=100)
	// made this a String because thought it might be a range (e.g. 10-12 reps)
	// or something more descriptive (e.g. 10 on each leg)
	// or a length (e.g. plank 60 seconds), but that was just a thought, could change to int
	private String recommendedReps;
	
	@Column
	// duration in seconds
	private int expectedDuration;
	
	@Column
	private int difficulty;
	
	@Column
	private int noiseLevel;

	@Basic(fetch = FetchType.LAZY)
	@Lob
	private byte[][] pictures;
	
	@ManyToMany
	private List<MusGroup> _muscleGroups;
	

	/**
	 * for use by Hibernate hydration
	 */
	public Exrcise(){}
	
	public Exrcise(String name){
		setName(name);
	}
	
	public String getName() {
		return _name;
	}

 	public void setName(String name) {
        this._name = name;
    }

    public String getDescription() {
		return description;
	}

    public void setDescription(String description) {
		this.description = description;
	}
 
	public byte[] getDemoVideo() {
		return demoVideo;
	}

	public void setDemoVideo(byte[] demoVideo) {
		this.demoVideo = demoVideo;
	}

	/**
	 * @return the muscleGroups
	 */
	public List<MusGroup> getMuscleGroups() {
		if (_muscleGroups==null) _muscleGroups=new ArrayList<MusGroup>();
		return _muscleGroups;
	}

	/**
	 * @param muscleGroup
	 */
	public void addMuscleGroup(MusGroup muscleGroup) {
		getMuscleGroups().add(muscleGroup);
	}
	
	public void setTips(String tips){
		this.tips = tips;
	}
	
	public String getTips(){
		return this.tips;
	}
	
	public void setRecommendedReps(String reps) {
		this.recommendedReps = reps;
	}
	
	public String getRecommendedReps() {
		return this.recommendedReps;
	}
	
	public void setExpectedDuration(int duration) {
		this.expectedDuration = duration;
	}
	
	public int getExpectedDuration() {
		return this.expectedDuration;
	}
	
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}
	
	public int getDifficulty() {
		return this.difficulty;
	}
	
	public void setNoiseLevel(int noiseLevel) {
		this.noiseLevel = noiseLevel;
	}
	
	public int getNoiseLevel() {
		return this.noiseLevel;
	}
	
    @Override
    public String toString() {
        return "Exercise [id=" + id + ", name=" + _name + "]";
    }

}
