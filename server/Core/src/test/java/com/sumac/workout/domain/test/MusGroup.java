/**
 * 
 */
package com.sumac.workout.domain.test;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.sumac.workout.domain.DomainModel;

/**
 * Sample class for testing hibernate mapping, base domain model classes, and base DAOs
 * @author PaulH
 *
 */
@Entity
public class MusGroup extends DomainModel {

    @Column(unique=true, nullable=false, length=200)
	private String _name;

    
	/**
	 * for use by Hibernate hydration
	 */
	public MusGroup(){}
	
    
	/**
	 * 
	 */
	public MusGroup(String name) {
		setName(name);
	}

	
	public String getName(){
		return _name;
	}
	public void setName(String name) {
		_name=name;
	}

}
