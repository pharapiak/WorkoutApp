package com.sumac.workout.hibernate.test;

import com.sumac.workout.domain.test.Exrcise;
import com.sumac.workout.hibernate.HibernateDataStore;

/**
 * Test class for testing base datastore
 * @author PaulH
 *
 */
public class ExrciseDataStore extends HibernateDataStore<Exrcise> {

	@Override
	protected String getTableName() {
		// TODO how can we get this from the Exercise annotation?
		return "Exrcise";
	}

}
