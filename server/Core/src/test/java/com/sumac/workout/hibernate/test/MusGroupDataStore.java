package com.sumac.workout.hibernate.test;

import com.sumac.workout.domain.test.MusGroup;
import com.sumac.workout.hibernate.HibernateDataStore;


/**
 * Test class for testing base datastore
 * @author PaulH
 *
 */
public class MusGroupDataStore extends HibernateDataStore<MusGroup> {

	@Override
	protected String getTableName() {
		// TODO how can we get this from the MuscleGroup annotation?
		return "MusGroup";
	}

}
