package com.sumac.workout.services.test;

import com.sumac.workout.domain.test.Exrcise;
import com.sumac.workout.hibernate.test.ExrciseDataStore;
import com.sumac.workout.services.DataAccessObject;
import com.sumac.workout.services.DataStore;

public class ExerciseTestDao extends DataAccessObject<Exrcise> {
	
	public ExerciseTestDao(DataStore<Exrcise> dataStore) {
		super(dataStore!=null?dataStore:new ExrciseDataStore());
	}
}
