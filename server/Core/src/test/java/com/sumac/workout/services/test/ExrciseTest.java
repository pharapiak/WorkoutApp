/**
 * 
 */
package com.sumac.workout.services.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Test;

import com.sumac.workout.domain.test.Exrcise;
import com.sumac.workout.domain.test.MusGroup;
import com.sumac.workout.hibernate.test.ExrciseDataStore;
import com.sumac.workout.hibernate.test.MusGroupDataStore;



/**
 * Test the storage and query of Exercise
 *
 */
public class ExrciseTest {

	/**
	 * Test method for {@link Exrcise.model.Exercise#Exercise(java.lang.String)}.
	 */
	
	@Test
	public void testExercise() {
		String name = "Up-downs";
		Exrcise exercise =  new Exrcise(name);
		assertEquals("Got the same name!",name,exercise.getName());
	}
	
	@Test
	public void testExerciseStorage() throws Exception {
		int testId=new Random().nextInt();
		
		Exrcise exercise =  new Exrcise("up-downs"+testId);
		exercise.setDescription("very hard");
		
		MusGroup mg1=new MusGroup("Glutes"+testId);
		MusGroup mg2=new MusGroup("Hamstrings"+testId);
		MusGroupDao mgServices=new MusGroupDao(new MusGroupDataStore());
		mgServices.save(mg1);
		mgServices.save(mg2);
		
		exercise.addMuscleGroup(mg1);
		exercise.addMuscleGroup(mg2);
		
		ExerciseTestDao services=new ExerciseTestDao(new ExrciseDataStore());
		long id = services.save(exercise);
		assertTrue("got an id",id>0);
		
		Exrcise fetchedExercise=services.findById(id);
		assertNotNull("got an exercise from db",fetchedExercise);
		assertEquals("got musclegroups in exercise", 2,fetchedExercise.getMuscleGroups().size());
		
		assertEquals("fetched has same name",exercise.getName(),fetchedExercise.getName());		
		assertEquals("fetched has same description",exercise.getDescription(),fetchedExercise.getDescription());		
	}

}
