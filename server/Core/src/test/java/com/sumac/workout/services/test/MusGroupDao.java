package com.sumac.workout.services.test;

import com.sumac.workout.domain.test.MusGroup;
import com.sumac.workout.hibernate.test.MusGroupDataStore;
import com.sumac.workout.services.DataAccessObject;
import com.sumac.workout.services.DataStore;

public class MusGroupDao extends DataAccessObject<MusGroup> {
	
	public MusGroupDao(DataStore<MusGroup> dataStore) {
		super(dataStore!=null?dataStore:new MusGroupDataStore());
	}
}
