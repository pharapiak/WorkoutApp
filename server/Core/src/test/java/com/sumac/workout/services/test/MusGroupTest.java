/**
 * 
 */
package com.sumac.workout.services.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Test;

import com.sumac.workout.domain.test.MusGroup;
import com.sumac.workout.hibernate.test.MusGroupDataStore;


/**
 * Test the storage and query of MuscleGroup
 *
 */
public class MusGroupTest {

	
	@Test
	public void testMuscleGroupStorage() throws Exception {
		int testId=new Random().nextInt();
		
		MusGroup obj1 =  new MusGroup("triceps"+testId);
		
		MusGroupDao services=new MusGroupDao(new MusGroupDataStore());
		long id = services.save(obj1);
		assertTrue("got an id",id>0);
		
		MusGroup fetchedObj=services.findById(id);
		assertNotNull("got an exercise from db",fetchedObj);
		
		assertEquals("fetched has same name",obj1.getName(),fetchedObj.getName());		
	}

}
