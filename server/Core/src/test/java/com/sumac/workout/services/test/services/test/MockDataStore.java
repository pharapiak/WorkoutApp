package com.sumac.workout.services.test.services.test;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sumac.workout.domain.DomainModel;
import com.sumac.workout.services.DataStore;

import javassist.tools.rmi.ObjectNotFoundException;



public abstract class MockDataStore<DomainModelType extends DomainModel> implements DataStore<DomainModelType> {

    private Map<Long,DomainModelType> _database;
    private static long _maxId=0;
    
    public Map<Long,DomainModelType> getDatabase() {
    	if (_database==null){
    		_database = new HashMap<Long,DomainModelType>();
    	}
		return _database;
    }
    
		/* (non-Javadoc)
	 * @see com.sumac.workout.hibernate.IDataStore#store(DomainModelType)
	 */
	public long store(DomainModelType obj) throws Exception {
		if (obj.getId()==0) {
			obj.setId(++_maxId);
		}
		getDatabase().put(obj.getId(),obj);
		return obj.getId();
	}

	/* (non-Javadoc)
	 * @see com.sumac.workout.hibernate.IDataStore#findAll()
	 */
	public Iterator<DomainModelType> findAll() {
		return getDatabase().values().iterator();
	}

	/* (non-Javadoc)
	 * @see com.sumac.workout.hibernate.IDataStore#findById(long)
	 */
	public DomainModelType findById(long id) throws ObjectNotFoundException {
		if (getDatabase().containsKey(id)) {
			return getDatabase().get(id);
		}
		throw new ObjectNotFoundException(String.format("failed to find object of type {0} with id {1}",getTableName(), id));
	}
	
	protected String getTableName(){
		return getEntityClass().getSimpleName();
	}

	@SuppressWarnings("unchecked")
	protected Class<DomainModelType> getEntityClass() {
		return (Class<DomainModelType>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

}
