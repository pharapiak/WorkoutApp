/**
 * 
 */
package com.sumac.workout.exercise.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.sumac.workout.domain.DomainModel;

/**
 *
 */
@Entity
public class MuscleGroup extends DomainModel {

    @Column(unique=true, nullable=false, length=200)
	private String _name;

    
	/**
	 * for use by Hibernate hydration
	 */
	public MuscleGroup(){}
	
    
	/**
	 * 
	 */
	public MuscleGroup(String name) {
		setName(name);
	}

	
	public String getName(){
		return _name;
	}
	public void setName(String name) {
		_name=name;
	}

}
