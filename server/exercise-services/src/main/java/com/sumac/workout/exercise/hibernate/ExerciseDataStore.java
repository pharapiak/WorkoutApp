package com.sumac.workout.exercise.hibernate;

import com.sumac.workout.exercise.domain.Exercise;
import com.sumac.workout.hibernate.HibernateDataStore;

public class ExerciseDataStore extends HibernateDataStore<Exercise> {

	@Override
	protected String getTableName() {
		// TODO how can we get this from the Exercise annotation?
		return "Exercise";
	}

}
