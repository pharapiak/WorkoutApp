package com.sumac.workout.exercise.hibernate;

import com.sumac.workout.exercise.domain.MuscleGroup;
import com.sumac.workout.hibernate.HibernateDataStore;

public class MuscleGroupDataStore extends HibernateDataStore<MuscleGroup> {

	@Override
	protected String getTableName() {
		// TODO how can we get this from the MuscleGroup annotation?
		return "MuscleGroup";
	}

}
