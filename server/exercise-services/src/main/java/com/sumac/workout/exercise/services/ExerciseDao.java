package com.sumac.workout.exercise.services;

import com.sumac.workout.exercise.domain.Exercise;
import com.sumac.workout.exercise.hibernate.ExerciseDataStore;
import com.sumac.workout.services.DataAccessObject;
import com.sumac.workout.services.DataStore;

public class ExerciseDao extends DataAccessObject<Exercise> {
	
	public ExerciseDao(DataStore<Exercise> dataStore) {
		super(dataStore!=null?dataStore:new ExerciseDataStore());
	}
}
