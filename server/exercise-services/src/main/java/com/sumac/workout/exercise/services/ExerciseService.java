package com.sumac.workout.exercise.services;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sumac.workout.exercise.domain.Exercise;
import com.sumac.workout.exercise.hibernate.ExerciseDataStore;
import com.sumac.workout.exercise.services.ExerciseDao;
import com.sumac.workout.services.CreateException;
import com.sumac.workout.services.JsonError;
import com.sumac.workout.services.NotFoundException;

import javassist.tools.rmi.ObjectNotFoundException;


@Path("/exercises")
public class ExerciseService {
	
	private ExerciseDao _dao;

	/**
	 * This is the data access object that will be used to find and store instances
	 * @return
	 */
	public ExerciseDao getDao() {
		if (_dao==null){
			_dao=new ExerciseDao(new ExerciseDataStore());
		}
		return _dao;
	}
	
	public void setDao(ExerciseDao dao) {
		_dao=dao;
	}
	
    /**
     * get all registered ExerciseService
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
//    @Path("/all")
//    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.TEXT_PLAIN)
    public Exercise[]  get() {
		Iterator<Exercise> all=getDao().findAll();
		ArrayList<Exercise> result =  new ArrayList<Exercise>();
		all.forEachRemaining(result::add);
		return result.toArray(new Exercise[0]);
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Exercise get(@PathParam("id")long id) {
    	try {
    		return getDao().findById(id);
    	} catch (ObjectNotFoundException ex) {
            throw new NotFoundException(new JsonError("Error", "Exercise " + id + " not found"));
    	}
    }
	
  @POST
  @Path("/add")
  @Produces(MediaType.APPLICATION_JSON)
  public Response addExercise(Exercise newObj){
	try{
		getDao().save(newObj);
	    return Response.status(201).build();
    } catch (Exception ex) {
    	throw new CreateException(new JsonError("Error","Failed to create exercise;" + ex.getMessage()));
    }
  }
  
  /*	
  @PUT
  @Path("{id}/update")
  @Produces(MediaType.APPLICATION_JSON)
  public Response updateExercise(Exercise customer){
    int matchIdx = 0;
    Optional<Exercise> match = cList.stream()
        .filter(c -> c.getId() == customer.getId())
        .findFirst();
    if (match.isPresent()) {
      matchIdx = cList.indexOf(match.get());
      cList.set(matchIdx, customer);
      return Response.status(Response.Status.OK).build();
    } else {
      return Response.status(Response.Status.NOT_FOUND).build();      
    }
  }
  
  @DELETE
  @Path("/remove/{id}")
  public Response deleteExercise(@PathParam("id") long id){
    Predicate <Exercise> exercise = c -> c.getId() == id;
    if (cList.removeIf(exercise)) {
        return Response.status(Response.Status.OK).build();     
    }else {
      return Response.status(Response.Status.NOT_FOUND).build();      
    }
  }
*/    
}
