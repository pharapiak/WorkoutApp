package com.sumac.workout.exercise.services;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sumac.workout.exercise.domain.Exercise;


@Path("/exercises-mock")
public class ExerciseServiceMock {
	

    /**
     * get all registered ExerciseServiceMock
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.TEXT_PLAIN)
    public List<Exercise>  get() {
    	Exercise[] all = {new Exercise("Up Downs"),new Exercise("Push-ems")};
		return Arrays.asList(all);
//		return all.toArray(new Exercise[0]); //TODO use iterator

    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Exercise get(@PathParam("id")long id) {
    		return new Exercise("Exercise id "+id);
    }
	
/*	
  @POST
  @Path("/add")
  @Produces(MediaType.APPLICATION_JSON)
  public Response addExercise(Exercise customer){
    cList.add(customer);
    return Response.status(201).build();
  }
  
  @PUT
  @Path("{id}/update")
  @Produces(MediaType.APPLICATION_JSON)
  public Response updateExercise(Exercise customer){
    int matchIdx = 0;
    Optional<Exercise> match = cList.stream()
        .filter(c -> c.getId() == customer.getId())
        .findFirst();
    if (match.isPresent()) {
      matchIdx = cList.indexOf(match.get());
      cList.set(matchIdx, customer);
      return Response.status(Response.Status.OK).build();
    } else {
      return Response.status(Response.Status.NOT_FOUND).build();      
    }
  }
  
  @DELETE
  @Path("/remove/{id}")
  public Response deleteExercise(@PathParam("id") long id){
    Predicate <Exercise> exercise = c -> c.getId() == id;
    if (cList.removeIf(exercise)) {
        return Response.status(Response.Status.OK).build();     
    }else {
      return Response.status(Response.Status.NOT_FOUND).build();      
    }
  }
*/    
}
