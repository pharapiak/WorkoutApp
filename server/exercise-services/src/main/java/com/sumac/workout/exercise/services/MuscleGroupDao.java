package com.sumac.workout.exercise.services;

import com.sumac.workout.exercise.domain.MuscleGroup;
import com.sumac.workout.exercise.hibernate.MuscleGroupDataStore;
import com.sumac.workout.services.DataAccessObject;
import com.sumac.workout.services.DataStore;

public class MuscleGroupDao extends DataAccessObject<MuscleGroup> {
	
	public MuscleGroupDao(DataStore<MuscleGroup> dataStore) {
		super(dataStore!=null?dataStore:new MuscleGroupDataStore());
	}
}
