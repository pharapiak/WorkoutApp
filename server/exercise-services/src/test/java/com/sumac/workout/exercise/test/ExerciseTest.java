/**
 * 
 */
package com.sumac.workout.exercise.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Test;

import com.sumac.workout.exercise.domain.Exercise;
import com.sumac.workout.exercise.domain.MuscleGroup;
import com.sumac.workout.exercise.hibernate.ExerciseDataStore;
import com.sumac.workout.exercise.hibernate.MuscleGroupDataStore;
import com.sumac.workout.exercise.services.ExerciseDao;
import com.sumac.workout.exercise.services.MuscleGroupDao;

/**
 * Test the storage and query of Exercise
 *
 */
public class ExerciseTest {

	/**
	 * Test method for {@link workoutapp.model.Exercise#Exercise(java.lang.String)}.
	 */
	
	@Test
	public void testExercise() {
		String name = "Up-downs";
		Exercise exercise =  new Exercise(name);
		assertEquals("Got the same name!",name,exercise.getName());
	}
	
	@Test
	public void testExerciseStorage() throws Exception {
		int testId=new Random().nextInt();
		Exercise exercise =  new Exercise("up-downs"+testId);
		exercise.setDescription("very hard");
		
		MuscleGroup mg1=new MuscleGroup("Glutes"+testId);
		MuscleGroup mg2=new MuscleGroup("Hamstrings"+testId);
		MuscleGroupDao mgServices=new MuscleGroupDao(new MuscleGroupDataStore());
		mgServices.save(mg1);
		mgServices.save(mg2);
		
		exercise.addMuscleGroup(mg1);
		exercise.addMuscleGroup(mg2);
		
		ExerciseDao services=new ExerciseDao(new ExerciseDataStore());
		long id = services.save(exercise);
		assertTrue("got an id",id>0);
		
		Exercise fetchedExercise=services.findById(id);
		assertNotNull("got an exercise from db",fetchedExercise);
		assertEquals("got musclegroups in exercise", 2,fetchedExercise.getMuscleGroups().size());
		
		assertEquals("fetched has same name",exercise.getName(),fetchedExercise.getName());		
		assertEquals("fetched has same description",exercise.getDescription(),fetchedExercise.getDescription());		
	}

}
