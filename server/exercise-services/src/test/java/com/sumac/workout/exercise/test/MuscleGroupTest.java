/**
 * 
 */
package com.sumac.workout.exercise.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sumac.workout.exercise.domain.MuscleGroup;
import com.sumac.workout.exercise.hibernate.MuscleGroupDataStore;
import com.sumac.workout.exercise.services.MuscleGroupDao;

/**
 * Test the storage and query of MuscleGroup
 *
 */
public class MuscleGroupTest {

	
	@Test
	public void testMuscleGroupStorage() throws Exception {
		MuscleGroup obj1 =  new MuscleGroup("triceps");
		
		MuscleGroupDao services=new MuscleGroupDao(new MuscleGroupDataStore());
		long id = services.save(obj1);
		assertTrue("got an id",id>0);
		
		MuscleGroup fetchedObj=services.findById(id);
		assertNotNull("got an exercise from db",fetchedObj);
		
		assertEquals("fetched has same name",obj1.getName(),fetchedObj.getName());		
	}

}
