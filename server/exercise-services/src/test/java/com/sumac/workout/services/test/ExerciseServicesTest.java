package com.sumac.workout.services.test;


import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.sumac.workout.exercise.services.Main;


public class ExerciseServicesTest {

    private HttpServer server;
    private WebTarget target;
    
    //note the port change, so unit tests don't collide with real services
    public static final String BASE_URI = "http://localhost:8081/workout/";
    
    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer("BASE_URI");
        // create the client
        Client c = ClientBuilder.newClient();

   //for json support
//        c.getConfiguration().enable(new com.fasterxml.jackson.core.json.());

        target = c.target(BASE_URI);
        
//      WebResource webResource = createRestClient(true).resource(
//      REST_BASE_PATH + "/service");
//
//ClientResponse response = webResource.type(MediaType.APPLICATION_JSON)
//     .accept(MediaType.APPLICATION_JSON)
//     .post(ClientResponse.class, contentActionObject);       
    }

    @After
    public void tearDown() throws Exception {
        server.shutdownNow();
    }

    /**
     * Test to see that the message "Got it!" is sent in the response.
     */
    @Test
    public void testGetIt() {
    	String responseMsg="";
//        String responseMsg = target.path("exercises/getAll").request().get(String.class);
    	try {
//    	    responseMsg = target.path("exercises/getAll").request().accept(MediaType.APPLICATION_JSON).get(String.class);
    	    responseMsg = target.path("exercises").request().get(String.class);
    	} catch (Exception ex) {
    		Assert.fail("some problem"+ex.getMessage());
    	}
        
//        WebResource webResource = createRestClient(true).resource(
//                REST_BASE_PATH + "/service");
//
//   ClientResponse response = webResource.type(MediaType.APPLICATION_JSON)
//               .accept(MediaType.APPLICATION_JSON)
//               .post(ClientResponse.class, contentActionObject);
        
        assertEquals("Got it!", responseMsg);
    }
}

